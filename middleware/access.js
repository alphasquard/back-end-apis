const path = require('path'),
  JWT = require(path.resolve('./lib/jwt')),
  errorHandler = require(path.resolve('helpers/error-handler')),
  Applicant = require(path.resolve('./models/applicants.model')),
  Admin = require(path.resolve('./models/admins.model'));
//   Employee = require(path.resolve('./models/employees.model'));

module.exports = {

    verifyApplicant: async function(req, res, next) {
        try {
            let isValidToken = JWT.verifyJwtToken(req);
            if(!isValidToken || isValidToken.aud != global.config.roles.applicant) {
                let err = { name:  'UnauthorizedError' };
                return errorHandler(err, req, res, next);
            }
            let applicant = await Applicant.findOne({
                _id: isValidToken._id
            }).lean().exec();
            if (applicant) {
                req.user = applicant;
                next();
            } else {
                let err = { name:  'UnauthorizedError' };
                return errorHandler(err, req, res, next);
            }
        } catch (err) {
            return errorHandler(err, req, res, next);
        }
    },
    verifyAdmin: async function(req, res, next) {
        try {
            let isValidToken = JWT.verifyJwtToken(req);
            if(!isValidToken || isValidToken.aud != global.config.roles.admin) {
                let err = { name:  'UnauthorizedError' };
                return errorHandler(err, req, res, next);
            }
            let admin = await Admin.findOne({
                _id: isValidToken._id
            }).lean().exec();
            if (admin) {
                req.user = admin;
                next();
            } else {
                let err = { name:  'UnauthorizedError' };
                return errorHandler(err, req, res, next);
            }
        } catch (err) {
            return errorHandler(err, req, res, next);
        }
    }

}
