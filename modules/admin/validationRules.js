var ValidationRules = {

    register: {
        userName:'required|string',
        password: 'required|string'
    }, 
    section: {
        slug: 'required'
    },
    //Work Experience
    workExperience: {
        label: 'required|min:2|string',
        companyId: 'required'
    },

    //Department
    department: {
        name: 'required|min:2|string',
        companyId: 'required'

    },

    //FAQs
    faq: {
        question: 'required|min:2|string',
        answer: 'required|min:2|string',
        companyId: 'required'
    },

    //CMS Page
    cmsPage: {
        name: 'required|min:2|string',
        slug: 'required|min:2|string',
        companyId: 'required'
    },
    updateCMSPage: {
        name: 'required|min:2|string',
        companyId: 'required'

    },

    //Job Group Level
    jobGroupLevel: {
        name: 'required|min:2|string',
        requiredLevel: 'required',
        companyId: 'required'
    },

    //Location or Institutes
    location: {
        name: 'required|min:2|string',
        companyId: 'required'
    }

}

module.exports = ValidationRules;
