const express = require('express'),
  router = express.Router(),
  path = require('path'),
  bodyParserJSON = express.json(),
  bodyParserURLEncoded = express.urlencoded({ extended: true }),
  Access = require(path.resolve('./middleware/access')),
  JWT = require(path.resolve('./lib/jwt')),
  RegisterController = require('./controllers/registerController');

module.exports = (app) => {

  router.get('/me', Access.verifyAdmin, JWT.tokenValidation);
  router.get('/login',  RegisterController.index);
  router.post('/login',  RegisterController.signIn);


  app.use('/portal', router);
};
