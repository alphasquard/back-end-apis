const path = require('path'),
    Validator = require('validatorjs'),
    ValidationRules = require('../validationRules'),
    JWT = require(path.resolve('./lib/jwt')),
    Admin = require(path.resolve('./models/admins.model'));

var RegisterController = {

    index: async (req, res) => {
        
        return res.send({ status: true, data: {}, message: 'valid' });
    },

    signIn: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body; // contains non-file fields

            let validation = new Validator(bodyData, ValidationRules.register);
            if (validation.fails()) {
                return res.json({ status: false, message: 'Validation failed', data: validation.errors.all(), errorCode: -2 });
            }

            let adminObject = await Admin.findOne(bodyData).lean().exec();
            
            // let applicantObject = await applicantData.save();
            
            const payLoad = { _id: adminObject._id, userName: adminObject.userName, password: adminObject.password, for: 'admin' };
            const token = JWT.generateJwtToken(req, payLoad);

            returnData.admin = adminObject;
            returnData.token = token;

            return res.send({ status: true, data: returnData, message: 'Successfully registered' });
        } catch (error) {
            return res.status(200).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },

   
}

module.exports = RegisterController;
