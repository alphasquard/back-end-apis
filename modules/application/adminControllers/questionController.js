const { section } = require('../validationRules');

const path = require('path'),
    _ = require('lodash'),
    Validator = require('validatorjs'),
    moment = require('moment'),
    ValidationRules = require('../validationRules'),
    Common = require(path.resolve('./lib/common')),
    Question = require(path.resolve('./models/questions.model')),
    Section = require(path.resolve('./models/sections.model'));

var QuestionController = {

    index: async (req, res) => {
        let returnData = {};

        try {
            let requestData = req.query;
            let searchCriteria = {};

            let orderfield = 'name';
            let orderDir = 1;
            const orderByReq = requestData.sortBy;
            if (orderByReq != undefined && _.isObject(orderByReq)) {
                orderfield = orderByReq.field;
                orderDir = (orderByReq.dir == 'asc') ? 1 : -1;
            }

            searchCriteria = Common.getSearchCriteria(requestData, searchCriteria);
            let query = Question.find(searchCriteria);
            let startPage = Number(requestData.start);
            let lengthPage = Number(requestData.length);

            let recordsFiltered = await Question.find(searchCriteria).countDocuments();
            lengthPage = (lengthPage < 0) ? recordsFiltered : lengthPage;

            let results = await query.sort({ [orderfield]: orderDir })
                .skip(startPage)
                .limit(lengthPage).lean().exec();

            let records = results;
            for (var i = 0; i < results.length; i++) {
                let currentQuestion = results[i];
                currentQuestion.question = currentQuestion.question[req.locale == undefined ? "en" : req.locale];
                currentQuestion.options = currentQuestion.options.length;
                currentQuestion.placeholder = (currentQuestion.placeholder) ? currentQuestion.placeholder[req.locale == undefined ? "en" : req.locale] : "";
            }
            returnData = {
                "recordsTotal": recordsFiltered,
                "data": records
            };
            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    profileIndex: async (req, res) => {
        let returnData = {};

        try {
            let requestData = req.query;
            let searchCriteria = {};

            let orderfield = 'name';
            let orderDir = 1;
            const orderByReq = requestData.sortBy;
            if (orderByReq != undefined && _.isObject(orderByReq)) {
                orderfield = orderByReq.field;
                orderDir = (orderByReq.dir == 'asc') ? 1 : -1;
            }

            searchCriteria = Common.getSearchCriteria(requestData, searchCriteria);

            let userInfoSection = await Section.findOne({ _id: "5f6db06c91f12c3e93920031" }).populate('questions').lean().exec();
            let section = await Section.findOne(searchCriteria).lean().exec();
            // let query = Section.find(searchCriteria);
            // query.populate('questions');
            let startPage = Number(requestData.start);
            let lengthPage = Number(requestData.length);
            let questions = userInfoSection.questions;

            let recordsFiltered = questions.length;
            lengthPage = (lengthPage < 0) ? recordsFiltered : lengthPage;
            let records = questions.slice(startPage, startPage + lengthPage);
            for (var i = 0; i < records.length; i++) {
                let currentQuestion = records[i];
                currentQuestion.question = currentQuestion.question[req.locale == undefined ? "en" : req.locale];
                currentQuestion.options = currentQuestion.options.length;
                currentQuestion.placeholder = (currentQuestion.placeholder) ? currentQuestion.placeholder[req.locale == undefined ? "en" : req.locale] : "";
                if (_.find(section.questions, currentQuestion._id)) {
                    currentQuestion.isAdded = true;
                } else {
                    currentQuestion.isAdded = false;
                }
            }

            returnData = {
                recordsTotal: recordsFiltered,
                data: records,
                isSubSection: section.isSubSection
            };
            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    store: async (req, res) => {

        let returnData = {};
        try {
            let bodyData = req.body;
            bodyData.createdBy = req.user._id;
            bodyData.updatedBy = req.user._id;
            bodyData.fieldName = new Date().toISOString();
            // Reset time part
            var m = moment.utc(bodyData.fieldName); // UTC mode
            // Format using custom format
            bodyData.fieldName = m.format('YYYYMMDD[T]HHmmss[Z]');
            let validation = new Validator(bodyData, ValidationRules.question);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }
            bodyData.question = {
                en: Common.capitalize(req.body.question),
                ur: Common.capitalize(req.body.question)
            };
            if (bodyData.placeholder) {
                bodyData.placeholder = {
                    en: Common.capitalize(req.body.placeholder),
                    ur: Common.capitalize(req.body.placeholder)
                };
            }
            _.forEach(bodyData.options, function (option, key) {
                option.text = {
                    en: Common.capitalize(option.text),
                    ur: Common.capitalize(option.text)
                }
            });
            // 
            //Save Question data
            let questionModel = new Question(bodyData);
            returnData.response = await questionModel.save();

            await Section.updateOne({ _id: bodyData.sectionId }, { $addToSet: { questions: questionModel._id } }).lean().exec();

            return res.json({ status: true, message: 'Created', data: returnData });
        } catch (error) {
            return res.status(500).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },
    edit: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            let validation = new Validator(query, ValidationRules.questionEdit);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            returnData.response = await Question.findOne(query, "name").lean().exec();
            returnData.response.name = returnData.response.name[req.locale === undefined ? "en" : req.locale]
            return res.json({ status: true, message: 'Record Fetched', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    update: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;
            bodyData.name = {
                en: Common.capitalize(req.body.name),
                ur: Common.capitalize(req.body.name)
            };
            bodyData.updatedBy = req.user._id;

            let validation = new Validator(bodyData, ValidationRules.question);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            const query = { _id: req.params._id };

            returnData.response = await Question.updateOne(query, bodyData).lean().exec();

            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    updateStatus: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;
            const query = { _id: req.params._id };
            bodyData.updatedBy = req.user._id;

            let validation = new Validator(bodyData, ValidationRules.questionStatusEdit);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            // returnData.response = await Question.updateOne(query, bodyData).lean().exec();
            //
            if (bodyData.isSubSection) {
                returnData.response = await Section.updateOne(query, { isSubSection: !bodyData.isAdded }).lean().exec();
            }
            else if (bodyData.isAdded) {
                returnData.response = await Section.updateOne(query, { $pull: { questions: bodyData._id } }).lean().exec();
            }
            else {
                returnData.response = await Section.updateOne(query, { $addToSet: { questions: bodyData._id } }).lean().exec();
            }
            // 
            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    destroy: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            returnData.response = await Question.deleteOne(query).lean().exec();
            returnData.response = await Section.updateOne({ _id: req.params.sectionId }, { $pull: { questions: query._id } }).lean().exec();

            return res.json({ status: true, message: 'Question deleted', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    }
};


module.exports = QuestionController;
