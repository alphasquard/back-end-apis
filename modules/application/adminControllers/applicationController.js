const path = require('path'),
    _ = require('lodash'),
    Validator = require('validatorjs'),
    ValidationRules = require('../validationRules'),
    moment = require('moment'),
    Common = require(path.resolve('./lib/common')),
    Application = require(path.resolve('./models/applications.model')),
    Section = require(path.resolve('./models/sections.model')),
    Question = require(path.resolve('./models/questions.model'));

var ApplicationController = {

    index: async (req, res) => {
        let returnData = {};

        try {
            let requestData = req.query;
            let searchCriteria = {};

            let orderfield = 'name';
            let orderDir = 1;
            const orderByReq = requestData.sortBy;
            if (orderByReq != undefined && _.isObject(orderByReq)) {
                orderfield = orderByReq.field;
                orderDir = (orderByReq.dir == 'asc') ? 1 : -1;
            }

            searchCriteria = Common.getSearchCriteria(requestData, searchCriteria);
            let query = Application.find(searchCriteria);
            let startPage = Number(requestData.start);
            let lengthPage = Number(requestData.length);

            let recordsFiltered = await Application.find(searchCriteria).countDocuments();
            lengthPage = (lengthPage < 0) ? recordsFiltered : lengthPage;

            let results = await query.sort({ [orderfield]: orderDir })
                .skip(startPage)
                .limit(lengthPage).lean().exec();
            let records = [];
            for (i = 0; i < results.length; i++) {
                let section = results[i];
                section.sections = await Section.find({ applicationId: section._id }).countDocuments();
                records.push(section);
            }
            returnData = {
                "recordsTotal": recordsFiltered,
                "data": records
            };

            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    store: async (req, res) => {

        let returnData = {};
        try {
            let bodyData = req.body;
            bodyData.name = Common.capitalize(req.body.name);
            bodyData.createdBy = req.user._id;
            bodyData.updatedBy = req.user._id;

            let validation = new Validator(bodyData, ValidationRules.application);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            //Save Application data
            let applicationModel = new Application(bodyData);
            returnData.response = await applicationModel.save();
            const userInfoSection = await Section.findOne({ _id: "5f6db06c91f12c3e93920031" }).lean().exec();
            delete userInfoSection._id;
            //Save basic Section data
            let sectionModel = new Section(userInfoSection);
            sectionModel.applicationId = applicationModel._id;
            await sectionModel.save();
            console.log("f" + returnData.response._id);
            return res.json({ status: true, message: 'Created', data: returnData });
        } catch (error) {
            return res.status(500).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },
    edit: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            let validation = new Validator(query, ValidationRules.applicationEdit);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            returnData.application = await Application.findOne(query, "name year scholarshipType dueDate isPublish").lean().exec();
            const sections = await Section.find({ applicationId: req.params._id }, "questions isProfileSection").lean().exec();
            returnData.profileSection = {};
            returnData.section = {};
            _.forEach(sections, function (section, key) {
                if (section.isProfileSection) {
                    returnData.profileSection = section;
                } else {
                    returnData.section = section;
                }
            });
            return res.json({ status: true, message: 'Record Fetched', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    update: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;

            bodyData.updatedBy = req.user._id;
            // bodyData.dueDate = new Date().toISOString();
            let validation = new Validator(bodyData, ValidationRules.applicationUpdate);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }
            const query = { _id: req.params._id };

            returnData.response = await Application.updateOne(query, bodyData).lean().exec();

            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    destroy: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            returnData.response = await Application.deleteOne(query).lean().exec();

            return res.json({ status: true, message: 'Application deleted', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    duplicate: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            let validation = new Validator(query, ValidationRules.applicationEdit);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            const application = await Application.findOne(query).lean().exec();
            const sections = await Section.find({ applicationId: application._id }).lean().exec();

            delete application._id;
            application.isPublish = !application.isPublish;
            let applicationModel = new Application(application);
            let sectionList = [];

            for (let i = 0; i < sections.length; i++) {
                let section = sections[i];
                delete section._id;
                section.applicationId = applicationModel._id;
                let sectionModel = new Section(section);

                let sectionQuestions = [];
                if (section.isProfileSection) {
                    sectionQuestions = section.questions;
                } else {
                    const questions = await Question.find({ _id: { $in: section.questions } }).lean().exec();
                    for (let y = 0; y < questions.length; y++) {
                        let question = questions[y];
                        delete question._id;
                        // delete question.fieldName;
                        question.fieldName = new Date().toISOString();
                        var m = moment.utc(question.fieldName); // UTC mode
                        question.fieldName = m.format('YYYYMMDD[T]HHmmss[Z]');
                        question.sectionId = sectionModel._id;
                        let questionModel = new Question(question);
                        await questionModel.save();
                        sectionQuestions.push(questionModel._id);
                    }
                }


                sectionModel.questions = sectionQuestions;
                // sectionList.push(section);

                await sectionModel.save();
            }
            await Section.insertMany(sectionList);
            await applicationModel.save();
            return res.json({ status: true, message: 'Record Updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    detail: async (req, res) => {
        let returnData = {};

        try {
            let requestData = req.query;
            let searchCriteria = {};
            const applicationId = requestData.applicationId;
            let orderfield = 'name';
            let orderDir = 1;
            const orderByReq = requestData.sortBy;
            if (orderByReq != undefined && _.isObject(orderByReq)) {
                orderfield = orderByReq.field;
                orderDir = (orderByReq.dir == 'asc') ? 1 : -1;
            }

            searchCriteria = Common.getSearchCriteria(requestData, searchCriteria);
            let query = Section.find(searchCriteria);
            let startPage = Number(requestData.start);
            let lengthPage = Number(requestData.length);

            let recordsFiltered = await Section.find(searchCriteria).countDocuments();
            lengthPage = (lengthPage < 0) ? recordsFiltered : lengthPage;

            let results = await query.sort({ [orderfield]: orderDir })
                .skip(startPage)
                .limit(lengthPage).lean().exec();

            let records = results;
            let application = await Application.findOne({ _id: applicationId }, "section").lean().exec();

            for (var i = 0; i < results.length; i++) {
                let currentSection = results[i];
                if (_.find(application.section, currentSection._id)) {
                    currentSection.isAdded = true;
                } else {
                    currentSection.isAdded = false;
                }
                currentSection.name = currentSection.name[req.locale == undefined ? "en" : req.locale]
            }
            // _.forEach(feedback, function (value, key ) {
            //     statement += value + '\n ';
            // });

            // const playedChallnge = _.find(candidatePlayedChallenge, {challengeId: obj._id} );
            returnData = {
                "recordsTotal": recordsFiltered,
                "data": records
            };
            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    updateSection: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;
            let query = { _id: req.params._id };
            bodyData.updatedBy = req.user._id;

            let validation = new Validator(bodyData, ValidationRules.applicationSectionAdd);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            // returnData.package = await Package.updateOne({ _id: req.params._id }, { $addToSet: { modules: bodyData.module } }).lean().exec();
            // returnData.package = await Package.updateOne({ _id: req.params._id }, { $pull: { modules: bodyData.module } }).lean().exec();
            if (bodyData.isAdded) {
                returnData.response = await Application.updateOne(query, { $pull: { section: bodyData._id } }).lean().exec();

            } else {
                returnData.response = await Application.updateOne(query, { $addToSet: { section: bodyData._id } }).lean().exec();
            }

            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },

};


module.exports = ApplicationController;
