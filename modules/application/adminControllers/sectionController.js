const path = require('path'),
    _ = require('lodash'),
    Validator = require('validatorjs'),
    moment = require('moment'),
    ValidationRules = require('../validationRules'),
    Common = require(path.resolve('./lib/common')),
    Question = require(path.resolve('./models/questions.model')),
    Section = require(path.resolve('./models/sections.model'));

var SectionController = {

    index: async (req, res) => {
        let returnData = {};

        try {
            let requestData = req.query;
            let searchCriteria = {};

            let orderfield = 'name';
            let orderDir = 1;
            const orderByReq = requestData.sortBy;
            if (orderByReq != undefined && _.isObject(orderByReq)) {
                orderfield = orderByReq.field;
                orderDir = (orderByReq.dir == 'asc') ? 1 : -1;
            }

            searchCriteria = Common.getSearchCriteria(requestData, searchCriteria);
            // searchCriteria = {
            //     $or: [
            //         searchCriteria,
            //         { "slug": "profile_info" }
            //     ]
            // };
            let query = Section.find(searchCriteria);
            let startPage = Number(requestData.start);
            let lengthPage = Number(requestData.length);

            let recordsFiltered = await Section.find(searchCriteria).countDocuments();
            lengthPage = (lengthPage < 0) ? recordsFiltered : lengthPage;

            let results = await query.sort({ [orderfield]: orderDir })
                .skip(startPage)
                .limit(lengthPage).lean().exec();

            let records = results;
            for (var i = 0; i < results.length; i++) {
                let currentSection = results[i];
                currentSection.questions = currentSection.questions.length;
                currentSection.name = currentSection.name[req.locale == undefined ? "en" : req.locale]
            }
            returnData = {
                "recordsTotal": recordsFiltered,
                "data": records
            };
            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    store: async (req, res) => {

        let returnData = {};
        try {
            let sectionBodyData = req.body.section;
            let questionBodyData = req.body.questions;
            sectionBodyData.name = {
                en: Common.capitalize(sectionBodyData.name),
                ur: Common.capitalize(sectionBodyData.name)
            };
            sectionBodyData.createdBy = req.user._id;
            sectionBodyData.updatedBy = req.user._id;

            let validation = new Validator(sectionBodyData, ValidationRules.section);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }
            //Save Section data
            let sectionModel = new Section(sectionBodyData);
            sectionModel.questions = [];
            for (let i = 0; i < questionBodyData.length; i++) {
                let questionData = questionBodyData[i];
                questionData.createdBy = req.user._id;
                questionData.updatedBy = req.user._id;
                questionData.fieldName = new Date().toISOString();
                questionData.sectionId = sectionModel._id;

                // Reset time part
                var m = moment.utc(questionData.fieldName); // UTC mode
                // Format using custom format
                questionData.fieldName = m.format('YYYYMMDD[T]HHmmss[Z]')+i;
                questionData.question = {
                    en: Common.capitalize(questionData.question),
                    ur: Common.capitalize(questionData.question)
                };
                if (questionData.placeholder) {
                    questionData.placeholder = {
                        en: Common.capitalize(questionData.placeholder),
                        ur: Common.capitalize(questionData.placeholder)
                    };
                }
                _.forEach(questionData.options, function (option, key) {
                    option.text = {
                        en: Common.capitalize(option.text),
                        ur: Common.capitalize(option.text)
                    }
                });
                //Save Question data
                let questionModel = new Question(questionData);
                sectionModel.questions.push(questionModel._id);
                await questionModel.save();
            }
            

            returnData.response = await sectionModel.save();

            return res.json({ status: true, message: 'Created', data: returnData });
        } catch (error) {
            return res.status(500).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },
    edit: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            let validation = new Validator(query, ValidationRules.sectionEdit);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            returnData.section = await Section.findOne(query, "name").lean().exec();
            returnData.section.name = returnData.section.name[req.locale === undefined ? "en" : req.locale]
            let questions = await Question.find({ sectionId: req.params._id }).lean().exec();
            returnData.questions = [];
            for (var i = 0; i < questions.length; i++) {
                let currentQuestion = questions[i];
                currentQuestion.question = currentQuestion.question[req.locale == undefined ? "en" : req.locale];
                _.forEach(currentQuestion.options, function (option, key) {
                    option.text = option.text[req.locale == undefined ? "en" : req.locale];
                });
                currentQuestion.placeholder = (currentQuestion.placeholder) ? currentQuestion.placeholder[req.locale == undefined ? "en" : req.locale] : "";
                returnData.questions.push(currentQuestion);
            }
          
            return res.json({ status: true, message: 'Record Fetched', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    update: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;
            bodyData.name = {
                en: Common.capitalize(req.body.name),
                ur: Common.capitalize(req.body.name)
            };
            bodyData.updatedBy = req.user._id;

            let validation = new Validator(bodyData, ValidationRules.sectionUpdate);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            const query = { _id: req.params._id };

            returnData.response = await Section.updateOne(query, bodyData).lean().exec();

            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    destroy: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            returnData.response = await Section.deleteOne(query).lean().exec();

            return res.json({ status: true, message: 'Section deleted', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    }
};


module.exports = SectionController;
