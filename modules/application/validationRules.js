var ValidationRules = {

    register: {
        userName: 'required|string',
        password: 'required|string'
    },
    //Applicant
    application: {
        name: 'required|min:2|string',
        // year: 'required',
        scholarshipType: 'required',
        dueDate: 'required'
    },
    applicationEdit: {
        _id: 'required|string'
    },
    applicationUpdate: {
        name: 'required|min:2|string',
        // year: 'required',
        scholarshipType: 'required',
        dueDate: 'required'
    },
    applicationSectionAdd: {
        _id: 'required|string',
        isAdded: 'required'
    },
    //  section
    section: {
        name: 'required',
        // slug: 'required',
        applicationId: 'required'
    },
    sectionEdit: {
        _id: 'required|string'
    },
    sectionUpdate: {
        name: 'required',
        // slug: 'required',
    },
    // question
    question: {
        question: 'required',
        sectionId: 'required',
        type: 'required'
    },
    questionEdit: {
        _id: 'required|string'
    },
    questionStatusEdit: {
        _id: 'required|string',
        isAdded: 'required'
    },
    // applicant Application
    applicationApply: {
        applicationId: 'required|string'
    },
    applicationGet: {
        applicationId: 'required|string'
    },
    //Work Experience
    workExperience: {
        label: 'required|min:2|string',
        companyId: 'required'
    },



    //FAQs
    faq: {
        question: 'required|min:2|string',
        answer: 'required|min:2|string',
        companyId: 'required'
    },

    //CMS Page
    cmsPage: {
        name: 'required|min:2|string',
        slug: 'required|min:2|string',
        companyId: 'required'
    },
    updateCMSPage: {
        name: 'required|min:2|string',
        companyId: 'required'

    },

    //Job Group Level
    jobGroupLevel: {
        name: 'required|min:2|string',
        requiredLevel: 'required',
        companyId: 'required'
    },

    //Location or Institutes
    location: {
        name: 'required|min:2|string',
        companyId: 'required'
    }

}

module.exports = ValidationRules;
