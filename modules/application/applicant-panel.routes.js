const express = require('express'),
  router = express.Router(),
  path = require('path'),
  multer = require('multer'),
  crypto = require('crypto'),
  bodyParserJSON = express.json(),
  bodyParserURLEncoded = express.urlencoded({ extended: true }),
  Access = require(path.resolve('./middleware/access')),
  ApplicationController = require('./applicantControllers/applicationController');
const storage = multer.diskStorage({
  destination: './public/uploads/user',
  filename: function (req, file, cb) {
    // console.log("multer",file);
    crypto.pseudoRandomBytes(16, function (err, raw) {
      if (err) return cb(err)
      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})
const upload = multer({ storage: storage });
module.exports = (app) => {

  //Application
  router.get('/', ApplicationController.index);
  router.post('/:_id', ApplicationController.apply);
  router.get('/:_id', ApplicationController.edit);
  // applicantApplicationID
  router.put('/:_applicantApplicationId', upload.fields([{ name: 'file0', maxCount: 1 },{ name: 'file1', maxCount: 1 },{ name: 'file2', maxCount: 1 },{ name: 'file3', maxCount: 1 }]), bodyParserJSON, bodyParserURLEncoded, ApplicationController.update);
  router.delete('/:_id', ApplicationController.destroy);
  // router.get('/detail/', ApplicationController.detail);
  router.get('/duplicate/:_id', ApplicationController.duplicate);


  app.use('/api/application', Access.verifyApplicant, router);
};
