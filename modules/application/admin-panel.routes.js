const express = require('express'),
  router = express.Router(),
  path = require('path'),
  bodyParserJSON = express.json(),
  bodyParserURLEncoded = express.urlencoded({ extended: true }),
  Access = require(path.resolve('./middleware/access')),
  JWT = require(path.resolve('./lib/jwt')),
  ApplicationController = require('./adminControllers/applicationController'),
  SectionController = require('./adminControllers/sectionController'),
  QuestionController = require('./adminControllers/questionController');

module.exports = (app) => {
  //Question
  router.get('/question/', QuestionController.index);
  router.get('/question/details/', QuestionController.profileIndex);
  router.post('/question/', bodyParserJSON, bodyParserURLEncoded, QuestionController.store);
  router.get('/question/:_id', QuestionController.edit);
  router.put('/question/status/:_id', bodyParserJSON, bodyParserURLEncoded, QuestionController.updateStatus);
  router.put('/question/:_id', bodyParserJSON, bodyParserURLEncoded, QuestionController.update);
  router.delete('/question/:sectionId/:_id', QuestionController.destroy);
  
  //Section
  router.get('/section/', SectionController.index);
  router.post('/section/', bodyParserJSON, bodyParserURLEncoded, SectionController.store);
  router.get('/section/:_id', SectionController.edit);
  router.put('/section/:_id', bodyParserJSON, bodyParserURLEncoded, SectionController.update);
  router.delete('/section/:_id', SectionController.destroy);
  //Application
  router.get('/', ApplicationController.index);
  router.get('/duplicate/:_id', ApplicationController.duplicate);
  // router.get('/detail/', ApplicationController.detail);
  router.post('/', bodyParserJSON, bodyParserURLEncoded, ApplicationController.store);
  router.get('/:_id', ApplicationController.edit);
  router.put('/:_id', bodyParserJSON, bodyParserURLEncoded, ApplicationController.update);
  router.delete('/:_id', ApplicationController.destroy);
  
  // router.put('/add_section/:_id', bodyParserJSON, bodyParserURLEncoded, ApplicationController.updateSection);


  app.use('/portal/application', Access.verifyAdmin, router);
};
