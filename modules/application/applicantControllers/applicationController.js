const { forEach } = require('lodash');

const path = require('path'),
    _ = require('lodash'),
    Validator = require('validatorjs'),
    ValidationRules = require('../validationRules'),
    Common = require(path.resolve('./lib/common')),
    multer = require('multer'),
    crypto = require('crypto'),
    ApplicantApplication = require(path.resolve('./models/applicantApplications.model')),
    Application = require(path.resolve('./models/applications.model')),
    ProfileSection = require(path.resolve('./models/sectionsOutput.model')),
    Section = require(path.resolve('./models/sections.model'));

var ApplicationController = {

    index: async (req, res) => {
        let returnData = {};
        let searchCriteria = {};
        try {
            searchCriteria['dueDate'] = {
                // $lt: new Date().toISOString(),
                $gte: new Date().toISOString(),
            };
            const applications = await Application.find(searchCriteria, "name year scholarshipType dueDate isPublish").lean().exec();
            const applicantApplications = await ApplicantApplication.find({ applicantId: req.user._id }).populate("applicationId").lean().exec();
            returnData.openApplications = [];
            returnData.closeApplications = [];
            _.forEach(applicantApplications, function (application, key) {
                let item = {
                    _id: application.applicationId._id,
                    name: application.applicationId.name,
                    year: application.applicationId.year,
                    scholarshipType: application.applicationId.scholarshipType,
                    dueDate: application.applicationId.dueDate,
                    isStarted: true,
                    status: application.status,
                    isCompleted: application.isCompleted
                };

                if (application.applicationId.dueDate < new Date().toISOString()) {
                    item.isOpen = false;
                    returnData.closeApplications.push(item);
                }
                else {
                    item.isOpen = true;
                    returnData.openApplications.push(item);
                }
            })
            _.forEach(applications, function (application, key) {
                if (!_.find(returnData.openApplications, { _id: application._id })) {
                    application.isOpen = true;
                    application.isStarted = false;
                    application.status = "";
                    application.isCompleted = false;
                    // if application is publish then add
                    application.isPublish && returnData.openApplications.push(application);
                }

            });

            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    apply: async (req, res) => {

        let returnData = {};
        try {
            const query = { applicationId: req.params._id };
            let applicantApplicationData = query;

            let validation = new Validator(applicantApplicationData, ValidationRules.applicationApply);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }
            const profileQuestions = [];
            let profileSubSections = [];
            const profileSections = await ProfileSection.find({ applicantId: req.user._id }, "sections").lean().exec();
            profileSections.forEach(object => {
                _.forEach(object.sections, function (section, key) {
                    _.forEach(section.questions, function (question, key) {
                        profileQuestions.push({ _id: question._id, value: question.value });
                    });
                    // setting only one subsection(under development)
                    if (section.isSubSection) {
                        profileSubSections = section.subSections;
                    }
                });
            });
            // fetching application section
            const applicationSections = await Section.find(query, "questions isProfileSection isSubSection subSections").lean().exec();
            applicantApplicationData.applicantId = req.user._id;
            applicantApplicationData.createdBy = req.user._id;
            applicantApplicationData.updatedBy = req.user._id;
            applicantApplicationData.sections = [];
            applicantApplicationData.questions = [];
            _.forEach(applicationSections, function (section, key) {
                const resultSection = {
                    _id: section._id,
                    questions: [],
                    isSubSection: false,
                    isProfileSection: section.isProfileSection,
                    subSections: []
                };
                _.forEach(section.questions, function (question, index) {
                    let value = "";
                    // for assigning value to profile questions
                    if (section.isProfileSection) {
                        _.forEach(profileQuestions, function (pQuestion, index) {
                            if (pQuestion._id.toString() == question._id.toString()) {
                                value = pQuestion.value;
                            }
                        });
                    }
                    resultSection.questions.push({ _id: question._id, value: value });
                    // applicantApplicationData.questions.push({ sectionId: section._id, questionId: question._id });
                });
                if (section.isSubSection) {
                    resultSection.isSubSection = true;
                    resultSection.subSections = profileSubSections;
                }

                applicantApplicationData.sections.push(resultSection);
            });

            //Save Applicant Application data
            let applicationModel = new ApplicantApplication(applicantApplicationData);
            returnData.response = await applicationModel.save();
            return res.json({ status: true, message: 'Created', data: returnData });

        } catch (error) {
            console.log("error in apply", error);
            return res.status(500).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },
    edit: async (req, res) => {
        let returnData = {};
        try {

            const query = { applicationId: req.params._id, applicantId: req.user._id };

            let validation = new Validator(query, ValidationRules.applicationApply);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            returnData.response = await ApplicantApplication.findOne(query)
                // .populate("questions.sectionId")
                // .populate("sections._id")
                .populate("sections.questions._id")
                .populate("sections.subSections.questions._id")
                .lean().exec();

            _.forEach(returnData.response.sections, function (section, key) {
                _.forEach(section.questions, function (question, key) {
                    const questionObj = question._id;
                    questionObj.question = questionObj.question[req.locale == undefined ? "en" : req.locale];
                    questionObj.placeholder = questionObj.placeholder[req.locale == undefined ? "en" : req.locale];
                    _.forEach(questionObj.options, function (option, key) {
                        option.text = option.text[req.locale == undefined ? "en" : req.locale];
                    });
                });
            });

            return res.json({ status: true, message: 'Record Fetched', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    update: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;
            bodyData.questions = JSON.parse(bodyData.questions);
            bodyData.subSections = JSON.parse(bodyData.subSections);
            console.log("Application update",bodyData);
            // move in the last 
            if(bodyData.isProfileSection){
                ApplicationController.profileSectionUpdate(bodyData);
            }

            let questionsResponse = [];
            let imageFilesData = req.files; // contains file
            let files = [];
            _.forEach(imageFilesData, function (value, key) {
                files.push(value[0]);
            });
            console.log("Application update2",);

            let count = 0;
            _.forEach(bodyData.questions, function (value, key) {
                if (typeof value === 'object') {
                    questionsResponse.push({
                        _id: key,
                        file: files[count],
                        value: files[count].filename
                    });
                    count += 1;
                } else {
                    questionsResponse.push({
                        _id: key,
                        value: value
                    });
                }
            });

            const query = { _id: req.params._applicantApplicationId, "sections._id": bodyData.sectionId };
            console.log("Application update3",);

            await ApplicantApplication.updateOne({ _id: req.params._applicantApplicationId }, { isCompleted: bodyData.isCompleted }).lean().exec();
            returnData.response = await ApplicantApplication.updateOne(
                query, { $set: { "sections.$.questions": questionsResponse, "sections.$.subSections": bodyData.subSections } }).lean().exec();
                console.log("Application update4",);

                
            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            console.log("error in record update", e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    destroy: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            returnData.response = await Application.deleteOne(query).lean().exec();

            return res.json({ status: true, message: 'Application deleted', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    duplicate: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id };

            let validation = new Validator(query, ValidationRules.applicationEdit);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            const application = await Application.findOne(query).lean().exec();
            const sections = await Section.find({ applicationId: application._id }).lean().exec();

            delete application._id;
            application.isPublish = !application.isPublish;
            let applicationModel = new Application(application);
            let sectionList = [];
            await applicationModel.save();
            _.forEach(sections, function (section, key) {
                delete section._id;
                section.applicationId = applicationModel._id;
                sectionList.push(section)
            });
            await Section.insertMany(sectionList);

            return res.json({ status: true, message: 'Record Updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    detail: async (req, res) => {
        let returnData = {};

        try {
            let requestData = req.query;
            let searchCriteria = {};
            const applicationId = requestData.applicationId;
            let orderfield = 'name';
            let orderDir = 1;
            const orderByReq = requestData.sortBy;
            if (orderByReq != undefined && _.isObject(orderByReq)) {
                orderfield = orderByReq.field;
                orderDir = (orderByReq.dir == 'asc') ? 1 : -1;
            }

            searchCriteria = Common.getSearchCriteria(requestData, searchCriteria);
            let query = Section.find(searchCriteria);
            let startPage = Number(requestData.start);
            let lengthPage = Number(requestData.length);

            let recordsFiltered = await Section.find(searchCriteria).countDocuments();
            lengthPage = (lengthPage < 0) ? recordsFiltered : lengthPage;

            let results = await query.sort({ [orderfield]: orderDir })
                .skip(startPage)
                .limit(lengthPage).lean().exec();

            let records = results;
            let application = await Application.findOne({ _id: applicationId }, "section").lean().exec();

            for (var i = 0; i < results.length; i++) {
                let currentSection = results[i];
                if (_.find(application.section, currentSection._id)) {
                    currentSection.isAdded = true;
                } else {
                    currentSection.isAdded = false;
                }
                currentSection.name = currentSection.name[req.locale == undefined ? "en" : req.locale]
            }
            // _.forEach(feedback, function (value, key ) {
            //     statement += value + '\n ';
            // });

            // const playedChallnge = _.find(candidatePlayedChallenge, {challengeId: obj._id} );
            returnData = {
                "recordsTotal": recordsFiltered,
                "data": records
            };
            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    updateSection: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body;
            let query = { _id: req.params._id };
            bodyData.updatedBy = req.user._id;

            let validation = new Validator(bodyData, ValidationRules.applicationSectionAdd);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }

            // returnData.package = await Package.updateOne({ _id: req.params._id }, { $addToSet: { modules: bodyData.module } }).lean().exec();
            // returnData.package = await Package.updateOne({ _id: req.params._id }, { $pull: { modules: bodyData.module } }).lean().exec();
            if (bodyData.isAdded) {
                returnData.response = await Application.updateOne(query, { $pull: { section: bodyData._id } }).lean().exec();

            } else {
                returnData.response = await Application.updateOne(query, { $addToSet: { section: bodyData._id } }).lean().exec();
            }

            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    profileSectionUpdate: (data) => {
        console.log(data);
    }
};


module.exports = ApplicationController;
