const express = require('express'),
  router = express.Router(),
  path = require('path'),
  bodyParserJSON = express.json(),
  bodyParserURLEncoded = express.urlencoded({ extended: true }),
  Access = require(path.resolve('./middleware/access')),
  JWT = require(path.resolve('./lib/jwt')),
  RegisterController = require('./controllers/registerController'),
  ProfileController = require('./controllers/profileController');

module.exports = (app) => {

  router.get('/me', Access.verifyApplicant, JWT.tokenValidation);
  router.get('/register', RegisterController.index);
  router.post('/register', RegisterController.signIn);

  // router.post('/',  RegisterController.signIn);
  // router.get('/',  RegisterController.signIn);

  //Department
  // router.get('/department/', DepartmentController.index);
  // router.post('/department/', bodyParserJSON, bodyParserURLEncoded, DepartmentController.store);
  // router.put('/department/:_id', bodyParserJSON, bodyParserURLEncoded, DepartmentController.update);
  // router.post('/department/activate/', DepartmentController.updateStatus);
  // router.delete('/department/:_id', DepartmentController.destroy);
  router.post('/profile_section', Access.verifyApplicant, bodyParserJSON, bodyParserURLEncoded, ProfileController.store);
  router.get('/profile_section/:_id', Access.verifyApplicant, ProfileController.edit);

  app.use('/api', router);
};
