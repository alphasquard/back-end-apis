const path = require('path'),
    _ = require('lodash'),
    Validator = require('validatorjs'),
    ValidationRules = require('../validationRules'),
    Common = require(path.resolve('./lib/common')),
    SectionOutput = require(path.resolve('./models/sectionsOutput.model'));

var ProfileController = {

    index: async (req, res) => {
        let returnData = {};

        try {
            
            return res.json({ status: true, message: 'successful', data: returnData });

        } catch (e) {
            console.log(e)
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    edit: async (req, res) => {
        let returnData = {};
        try {

            const query = { applicantId: req.user._id, "sections._id": req.params._id };

            let validation = new Validator(query, ValidationRules.profileQuestion);
            if (validation.fails()) {
                return res.json({
                    status: false,
                    message: 'Validation failed',
                    data: validation.errors.all(),
                    errorCode: -2
                });
            }
            // returnData.response = await SectionOutput.findOne(query, { 'sections.$': 1 }).populate("sections.questions._id").lean().exec();

            const sectionOutput = await SectionOutput.findOne(query, { 'sections': 1 })
                .populate("sections.questions._id")
                .populate("sections.subSections.questions._id")
                // .populate({ 
                //     path: 'sections.subSections',
                //     populate: {
                //         path: '$._id',
                //         model: 'Question'
                //        }
                //  })
                .lean().exec();
            _.forEach(sectionOutput.sections, function (section, key) {
                if (section._id === req.params._id) {
                    let questions = section.questions;

                    _.forEach(questions, function (question, key) {
                        const questionObj = question._id;
                        questionObj.question = questionObj.question[req.locale == undefined ? "en" : req.locale];
                        questionObj.placeholder = questionObj.placeholder[req.locale == undefined ? "en" : req.locale];
                        _.forEach(questionObj.options, function (option, key) {
                            option.text = option.text[req.locale == undefined ? "en" : req.locale];
                        });
                    });
                   
                    returnData.response = section;
                }
            });

            return res.json({ status: true, message: 'Record Fetched', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    store: async (req, res) => {

        let returnData = {};
        try {
            let bodyData = req.body;
            let questionsResponse = [];
            const userId = req.user._id;
            let validation = new Validator(bodyData, ValidationRules.sectionOutput);
            if (validation.fails()) {
                return res.json({ status: false, message: 'Validation failed', data: validation.errors.all(), errorCode: -2 });
            }
         
            _.forEach(bodyData.questions, function (value, key) {
                questionsResponse.push({
                    _id: key,
                    value: value
                });
            });

            const query = { applicantId: userId, "sections._id": bodyData.slug };

            returnData.response = await SectionOutput.findOneAndUpdate(query,
            { $set: { "sections.$.questions": questionsResponse, "sections.$.subSections": bodyData.subSections } }
            ).lean().exec();

            return res.json({ status: true, message: 'Record Updated', data: returnData });
        } catch (error) {
            return res.status(500).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },
    update: async (req, res) => {
        let returnData = {};
        try {

            return res.json({ status: true, message: 'Record updated', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    },
    destroy: async (req, res) => {
        let returnData = {};
        try {

            const query = { _id: req.params._id, companyId: req.company._id };

            // await Department.deleteOne(query).lean().exec();

            return res.json({ status: true, message: 'Record deleted', data: returnData });

        } catch (e) {
            return res.json({ status: false, errCode: -1, message: e.message });
        }
    }
};


module.exports = ProfileController;
