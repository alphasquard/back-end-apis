const { sectionOutput } = require('../validationRules');

const path = require('path'),
    Validator = require('validatorjs'),
    ValidationRules = require('../validationRules'),
    JWT = require(path.resolve('./lib/jwt')),
    Applicant = require(path.resolve('./models/applicants.model')),
    SectionOutput = require(path.resolve('./models/sectionsOutput.model'));

var RegisterController = {

    index: async (req, res) => {

        return res.send({ status: true, data: {}, message: 'valid' });
    },

    signIn: async (req, res) => {
        let returnData = {};
        try {
            let bodyData = req.body; // contains non-file fields

            let validation = new Validator(bodyData, ValidationRules.register);
            if (validation.fails()) {
                return res.json({ status: false, message: 'Validation failed', data: validation.errors.all(), errorCode: -2 });
            }

            let isRegistered = await Applicant.findOne({ phoneNumber: bodyData.phoneNumber }).lean().exec();


            let applicantObject = await Applicant.findOneAndUpdate({ phoneNumber: bodyData.phoneNumber }, bodyData, { upsert: true, new: true }).lean().exec();
            if (!isRegistered) {
                //Get Profile Section data
                const profileInfoSection = await SectionOutput.findOne({ _id: "5f7dc0eae069302f3f2d5ba3" }).lean().exec();
                delete profileInfoSection._id;
                profileInfoSection.applicantId = applicantObject._id;
                //Save Profile Section data
                let sectionModel = new SectionOutput(profileInfoSection);
                await sectionModel.save();
            }


            const payLoad = { _id: applicantObject._id, phoneNumber: applicantObject.phoneNumber, for: 'applicant' };
            const token = JWT.generateJwtToken(req, payLoad);

            returnData.applicant = applicantObject;
            returnData.token = token;

            return res.send({ status: true, data: returnData, message: 'Successfully registered' });
        } catch (error) {
            return res.status(200).json({ status: false, errorCode: -1, data: returnData, message: error.message });
        }
    },


}

module.exports = RegisterController;
