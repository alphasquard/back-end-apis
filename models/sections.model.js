const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let languageCodes = config.locales;
let items = {};
for (let i = 0; i < languageCodes.length; i++) {
    items[languageCodes[i]] = { type: String, trim: true, required: true };
}
var SectionSchema = Schema(
    {
        name: items,//{ type: String, trim: true, required: true, index: true},
        applicationId: { type: Schema.ObjectId, ref: 'Application', index: true },
        slug: { type: String, trim: true, default: ''  },
        isSubSection: { type: Boolean, required: true, default: false },
        isProfileSection: { type: Boolean, required: true, default: false },
        questions: [{ type: Schema.ObjectId, ref: 'Question' }],
        subSection: [
            { type: Schema.ObjectId, ref: 'Section' }//Not in use 
        ],
        sortNo: { type: Number, required: true, default: 0 },
        status: { type: Boolean, required: true, default: true },
        createdBy: { type: Schema.ObjectId, ref: 'Admin', required: true },
        updatedBy: { type: Schema.ObjectId, ref: 'Admin', required: true },
    },
    { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);

module.exports = mongoose.model('Section', SectionSchema, 'Sections');
