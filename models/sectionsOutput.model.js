const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let languageCodes = config.locales;
let items = {};
for (let i = 0; i < languageCodes.length; i++) {
  items[languageCodes[i]] = { type: String, trim: true, required: true };
}
var SectionOutputSchema = Schema(
  {
    name: { type: String, trim: true, index: true },
    applicantId: { type: Schema.ObjectId, ref: 'Applicant', unique: true, index: true },
    sections: [
      {
        _id: { type: String, trim: true, default: "slug" },
        questions: [
          {
            _id: { type: Schema.ObjectId, ref: 'Question' },
            value: { type: String, default: '' },
          }],
        isSubSection: { type: Boolean, required: true, default: false },
        subSections: [
          {
            questions: [
              {
                _id: { type: Schema.ObjectId, ref: 'Question' },
                value: { type: String, default: '' },
              }],
          }
        ],
      }
    ],
    comments: [{ type: Object, default: {} }],
    isBaseSection: { type: Boolean, required: true, default: false },
    status: { type: Boolean, required: true, default: true },
    createdBy: { type: Schema.ObjectId, ref: 'Applicant', required: true },
    updatedBy: { type: Schema.ObjectId, ref: 'Applicant', required: true },
  },
  { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);

module.exports = mongoose.model('SectionOutput', SectionOutputSchema, 'SectionsOutputs');
