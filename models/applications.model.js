const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let languageCodes = config.locales;
let items = {};
for (let i = 0; i < languageCodes.length; i++) {
    items[languageCodes[i]] = { type: String, trim: true, required: true };
}
var ApplicationSchema = Schema(
    {
        // name: items,//{ type: String, trim: true, required: true, index: true},
        name: { type: String, trim: true, index: true },
        year: { type: String, trim: true },
        scholarshipType: { type: String, trim: true },
        dueDate: { type: String, trim: true },
        isPublish: { type: Boolean, required: true, default: false },
        section: [
            { type: Schema.ObjectId, ref: 'Section' }
        ],
        sortNo: { type: Number, required: true, default: 0 },
        status: { type: Boolean, required: true, default: true },
        createdBy: { type: Schema.ObjectId, ref: 'Admin', required: true },
        updatedBy: { type: Schema.ObjectId, ref: 'Admin', required: true },
    },
    { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);

module.exports = mongoose.model('Application', ApplicationSchema, 'Applications');
