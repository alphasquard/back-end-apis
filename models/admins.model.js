const mongoose = require('mongoose');

var AdminSchema = mongoose.Schema(
    {
        // name: { type: String, trim: true, required: true, index: true },
        userName: { type: String, trim: true, required: true , index: true},
        password: { type: String, trim: true, required: true },
        role: { type: String, trim: true, required: true, enum: ['admin', 'volunteer'], default: 'admin' },
    },
    { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);


module.exports = mongoose.model('Admin', AdminSchema, 'Admins');
