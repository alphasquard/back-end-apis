const mongoose = require('mongoose');

var ApplicantSchema = mongoose.Schema(
    {
        // name: { type: String, trim: true, required: true, index: true },
        phoneNumber: { type: String, trim: true, required: true , index: true},
        role: { type: String, trim: true, required: true, enum: ['applicant', 'volunteer'], default: 'applicant' },
    },
    { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);


module.exports = mongoose.model('Applicant', ApplicantSchema, 'Applicants');
