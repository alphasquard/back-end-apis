const mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var ApplicantApplicationSchema = Schema(
    {
        applicantId: { type: Schema.ObjectId, ref: 'Applicant', index: true },
        applicationId: { type: Schema.ObjectId, ref: 'Application', index: true },
        isCompleted: { type: Boolean, required: true, default: false },
        sections: [
            {
                _id: { type: Schema.ObjectId, ref: 'Section', index: true, required: true },
                questions: [
                    {
                        _id: { type: Schema.ObjectId, ref: 'Question' },
                        value: { type: String, default: '' },
                        file: { type: Object, default: '' },
                    }
                ],
                isSubSection: { type: Boolean, required: true, default: false },
                isProfileSection: { type: Boolean, required: true, default: false },
                subSections: [
                    {
                        questions: [
                            {
                                _id: { type: Schema.ObjectId, ref: 'Question' },
                                value: { type: String, default: '' },
                            }],
                    }
                ],
            }
        ],
        sortNo: { type: Number, required: true, default: 0 },
        status: { type: String, required: true, default: "started" },
        createdBy: { type: Schema.ObjectId, ref: 'Applicant', required: true },
        updatedBy: { type: Schema.ObjectId, ref: 'Applicant', required: true },
    },
    { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);

module.exports = mongoose.model('ApplicantApplication', ApplicantApplicationSchema, 'ApplicantApplications');
