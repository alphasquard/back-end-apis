const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let languageCodes = config.locales;
let items = {};
for (let i = 0; i < languageCodes.length; i++) {
    items[languageCodes[i]] = { type: String, trim: true };
}
var QuestionSchema = Schema(
    {
        fieldName: { type: String, trim: true, required: true, unique: true, default: 'question' },
        question: items,//{ type: String, trim: true, required: true, index: true},
        placeholder: items,
        sectionId: { type: Schema.ObjectId, ref: 'Section', index: true, required: true },
        type: { type: String, trim: true, required: true, enum: ['text', 'dropDown', 'textArea', 'radio', 'file'], default: 'text' },
        options: [{
            name: { type: String, required: true, trim: true, index: true },
            text: items,//{ type: String, trim: true, required: true},
        }],
        isRequired: { type: Boolean, required: true, default: true },
        sortNo: { type: Number, required: true, default: 0 },
        status: { type: Boolean, required: true, default: true },
        createdBy: { type: Schema.ObjectId, ref: 'Admin', required: true },
        updatedBy: { type: Schema.ObjectId, ref: 'Admin', required: true },
    },
    { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } }
);

module.exports = mongoose.model('Question', QuestionSchema, 'Questions');
