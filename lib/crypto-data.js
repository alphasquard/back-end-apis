const crypto = require('crypto'),
  algorithm = 'aes-256-cbc',
  privateKey = 'SABDyRGGDBAARyGABBBAByRDYDRYYSAS',
  //const privateKey = crypto.randomBytes(32);
  iv = crypto.randomBytes(16);

var CryptoData = {

    encrypt: function(text){
        try {
            var cipher = crypto.createCipher(algorithm, privateKey)
            var crypted = cipher.update(text,'utf8','hex')
            crypted += cipher.final('hex');
            return crypted;
        } catch (e) {
            console.log(e.message)
          return false;
      }
    },

    decrypt: function(text){
        try {
            var decipher = crypto.createDecipher(algorithm, privateKey)
            var dec = decipher.update(text,'hex','utf8')
            dec += decipher.final('utf8');
            return dec
        } catch (e) {
            console.log(e.message)
            return false;
        }
    },

    //let encrypPayLoad = CryptoData.encryptWithIv(Buffer.from(JSON.stringify(payLoad), "utf8"));
    //let encrypPayLoad = CryptoData.encryptWithIv(Buffer.from(payLoad, "utf8"));
    encryptWithIv: function(data) {
        try {
            let cipher = crypto.createCipheriv(algorithm, Buffer.from(privateKey), iv);
            var encrypted = Buffer.concat([
                //cipher.update(new Buffer(JSON.stringify(data), "utf8")),
                cipher.update(data),
                cipher.final()
            ]);
            /*let encrypted = cipher.update(data);
            encrypted = Buffer.concat([encrypted, cipher.final()]);*/
            return {
                iv: iv.toString('hex'),
                prv: encrypted.toString('hex')
            };
        } catch (e) {
            console.log(e.message)
            return false;
        }
    },

    decryptWithIv: function (data) {
        try {
            let iv = Buffer.from(data.iv, 'hex');
            let encryptedText = Buffer.from(data.prv, 'hex');
            let decipher = crypto.createDecipheriv(algorithm, Buffer.from(privateKey), iv);

            //var decrypted = Buffer.concat([decipher.update(data), decipher.final()]);

            let decrypted = decipher.update(encryptedText);
            decrypted = Buffer.concat([decrypted, decipher.final()]);
            return decrypted.toString();
        } catch (e) {
            console.log(e.message)
            return false;
        }
    },

}


module.exports = CryptoData;
