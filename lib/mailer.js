'use strict';

const nodeMailer = require('nodemailer');

module.exports = async function(data){
    try {
        const transporter = nodeMailer.createTransport({
            host: 'relay.ptml.pk',
            port: 25,
            secure: false,  //true for 465 port, false for other ports
            auth: 'none',/*{
            user: 'email@example.com',
            pass: 'password'
        }*/
        });

        let fromEmail = (data.from != undefined && data.from != '') ? data.from: global.config.email.from.email;
        let fromName = (data.fromUser != undefined && data.fromUser != '') ? data.name: global.config.email.from.name;
        let from = fromName + '<'+fromEmail+'>';
        const mailOptions = {
            from: from, // sender address
            to: data.to, // list of receivers
            subject: data.subject, // Subject line
            //text: 'Hello world?', // plain text body
            html: data.body // html body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                //console.log(error);
                //throw new Error(error);
                return false;
            } else {
                //console.log(info);
                return true;
            }
        });
    } catch (e) {
        //throw new Error(e);
        return false;
    }
}
