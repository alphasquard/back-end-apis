module.exports = async ({ to, from, fromUser, subject, locals, path }) => {

    const api_key = 'key-2de811a4363af8971134aa9885252bb0',
      domain = 'a2api.com',
      mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});


    let renderView = async function (p, l) {
        return new Promise(function (resolve, reject) {
            global.config.appObject.render(p, l, function (error, html) {
                if (error) reject(error);
                //console.log('html: ', html)
                resolve(html);
            });
        });
    };

    let sendMessage = async function (d) {
        return new Promise(function(resolve, reject) {
            mailgun.messages().send(d, function (error, body) {
                if (error) reject(error);
                //console.log('body: ', body)
                resolve(body);
            });
        });
    }

    let text = await renderView(path, locals);

    let data = {
        from: fromUser+' <'+from+'>',
        to,
        subject,
        html: text
    };

    return sendMessage(data);

}
