

var s3Functions = {
    retrieveFile: async (key, isEncode = false) => {
        return new Promise(function (resolve, reject) {
            const getParams = {
                Bucket: global.config.s3.bucket,
                Key: key
            };
            global.s3Object.getObject(getParams, function(err, data) {
                if (err) reject(err);
                
                const returnData = isEncode ? s3Functions.encode(data.Body) : data;
                resolve(returnData);
              });
        });
    },
    
    encode: (data) => {
        return Buffer.from(data, 'base64');
        /* let buf = Buffer.from(data);
        let base64 = buf.toString('base64');
        return base64; */
    },
};
module.exports = s3Functions;