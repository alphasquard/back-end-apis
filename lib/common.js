const path = require('path'),
    _ = require('lodash');
var Common = {

    arrFromObjKey: (dataArray, key) => {
        let returnArray = [];
        try {
            const arrLength = dataArray.length;
            for (let index = 0; index < arrLength; index++) {
                if (dataArray[index][key] !== undefined && dataArray[index][key] !== null)
                    returnArray.push(dataArray[index][key]);
            }
        } catch (e) {
            console.log(e)
        }
        return returnArray;
    },

    renderView: async (path, locals) => {
        return new Promise(function (resolve, reject) {
            global.config.appObject.render(path, locals, function (err, html) {
                if (err) reject(err);
                resolve(html);
            });
        });
    },

    parseQuery: (query) => {
        let search = query.trim();
        var args = search.split('&'); //search.substring(1).split('&');

        var argsParsed = {};

        var i, arg, kvp, key, value;

        for (i = 0; i < args.length; i++) {

            arg = args[i];

            if (-1 === arg.indexOf('=')) {

                argsParsed[decodeURIComponent(arg).trim()] = true;
            }
            else {

                kvp = arg.split('=');

                key = decodeURIComponent(kvp[0]).trim();

                value = decodeURIComponent(kvp[1]).trim();

                argsParsed[key] = value;
            }
        }

        return argsParsed;
    },

    capitalize: (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    },

    getSearchCriteria: (requestData, searchCriteria) => {

        // updated
        if (requestData.name !== undefined && requestData.name != '') {
            let searchQuery = requestData.name.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            const newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria['name'] = { $in: newRegesForSearch };
        }
        // application
        if (requestData.year !== undefined && requestData.year != '') {
            searchCriteria['year'] = requestData.year;
        }
        if (requestData.isPublish !== undefined && requestData.isPublish != '') {
            searchCriteria['isPublish'] = requestData.isPublish;
        }
        if (requestData.isOpen !== undefined && requestData.isOpen != '') {
            if (requestData.isOpen === 'true') {
                searchCriteria['dueDate'] = {
                    $gte: new Date().toISOString()
                };
            } else {
                searchCriteria['dueDate'] = {
                    $lt: new Date().toISOString()
                };
            }
        }
        if (requestData.scholarshipType !== undefined && requestData.scholarshipType != '') {
            searchCriteria['scholarshipType'] = requestData.scholarshipType;
        }
        // section
        if (requestData.slug !== undefined && requestData.slug != '') {
            searchCriteria['slug'] = requestData.slug;
        }
        if (requestData.isProfileSection !== undefined && requestData.isProfileSection != '') {
            searchCriteria['isProfileSection'] = requestData.isProfileSection;
        }
        if (requestData.applicationId !== undefined && requestData.applicationId != '') {
            searchCriteria['applicationId'] = requestData.applicationId;
        }
        // // question
        if (requestData.sectionId !== undefined && requestData.sectionId != '') {
            searchCriteria['sectionId'] = requestData.sectionId;
        }
        // // question
        if (requestData._id !== undefined && requestData._id != '') {
            searchCriteria['_id'] = requestData._id;
        }

        if (requestData.type !== undefined && requestData.type != '') {
            searchCriteria['type'] = requestData.type;
        }

        //only for candidate first and last name
        if (requestData.fullname != undefined && requestData.fullname != '') {
            let searchQuery = requestData.fullname.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            let newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria = {
                $or: [
                    { "firstName": { $in: newRegesForSearch } },
                    { "lastName": { $in: newRegesForSearch } }
                ]
            };
        }

        if (requestData.slug !== undefined && requestData.slug != '') {
            searchCriteria['slug'] = requestData.slug;
        }
        if (requestData.email != undefined && requestData.email != '') {
            searchCriteria['email'] = { "$regex": requestData.email, "$options": "i" };
        }
        if (requestData.cnic != undefined && requestData.cnic != '') {
            searchCriteria['cnicNo'] = { "$regex": requestData.cnic, "$options": "i" };
        }
        if (requestData.phone != undefined && requestData.phone != '') {
            searchCriteria['phone'] = requestData.phone;
        }
        if (requestData.location != undefined && requestData.location != '') {
            let searchQuery = requestData.location.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            let newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria = {
                $or: [
                    { "city": { $in: newRegesForSearch } },
                    { "country": { $in: newRegesForSearch } }
                ]
            };
        }
        if (requestData.city !== undefined && requestData.city != '') {
            let searchQuery = requestData.city.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            const newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria['city'] = { $in: newRegesForSearch };
        }
        if (requestData.country !== undefined && requestData.country != '') {
            let searchQuery = requestData.country.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            const newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria['country'] = { $in: newRegesForSearch };
        }
        if (requestData.sort_no !== undefined && requestData.sort_no != '') {
            searchCriteria['sort_no'] = Number(requestData.sort_no);
        }
        if (requestData.status !== undefined && requestData.status != '') {
            searchCriteria['status'] = requestData.status;
        }
        if (requestData.organization !== undefined && requestData.organization != '') {
            searchCriteria['organization'] = requestData.organization;
        }
        if (requestData.updatedAt !== undefined && requestData.updatedAt != '') {
            searchCriteria['updatedAt'] = requestData.updatedAt;
        }
        if (requestData.department != undefined && requestData.department != '') {
            searchCriteria['departmentId'] = requestData.department;
        }
        if (requestData.job_group_level != undefined && requestData.job_group_level != '') {
            searchCriteria['jobGroupLevelId'] = requestData.job_group_level;
        }
        if (requestData.work_experience != undefined && requestData.work_experience != '') {
            searchCriteria['workExperienceId'] = requestData.work_experience;
        }
        if (requestData.education_qualification != undefined && requestData.education_qualification != '') {
            searchCriteria['educationQualificationId'] = requestData.education_qualification;
        }
        if (requestData.isActive != undefined && requestData.isActive != '') {
            searchCriteria['isActive'] = requestData.isActive;
        }

        //for Logical Question
        if (requestData.questionHTML !== undefined && requestData.questionHTML != '') {
            let searchQuery = requestData.questionHTML.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            const newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria['questionHTML'] = { $in: newRegesForSearch };
        }
        // if (requestData.type !== undefined && requestData.type != '') {
        //     searchCriteria['type'] = requestData.type;
        // }
        if (requestData.subType !== undefined && requestData.subType != '') {
            searchCriteria['subType'] = requestData.subType;
        }
        if (requestData.seriesNo !== undefined && requestData.seriesNo != '') {
            searchCriteria['seriesNo'] = Number(requestData.seriesNo);
        }

        //for SJT Question
        if (requestData.competency != undefined && requestData.competency != '') {
            searchCriteria['competency'] = requestData.competency;
        }
        if (requestData.scenario !== undefined && requestData.scenario != '') {
            searchCriteria['scenario'] = requestData.scenario;
        }
        if (requestData.sub_component_competency != undefined && requestData.sub_component_competency != '') {
            searchCriteria['competencyId.competency'] = { "$regex": "/.*" + requestData.sub_component_competency + ".*/", "$options": "i" };
        }

        //for MBTI Question
        if (requestData.item != undefined && requestData.item != '') {
            searchCriteria['item'] = requestData.item;
        }

        //for Setting
        if (requestData.content !== undefined && requestData.content != '') {
            searchCriteria['content'] = requestData.content;
        }
        if (requestData.requiredLevel !== undefined && requestData.requiredLevel != '') {
            searchCriteria['requiredLevel'] = requestData.requiredLevel;
        }
        if (requestData.question !== undefined && requestData.question != '') {
            let searchQuery = requestData.question.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            const newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria['question'] = { $in: newRegesForSearch };
        }
        if (requestData.answer !== undefined && requestData.answer != '') {
            let searchQuery = requestData.answer.trim().replace(/  +/g, ' ').split(' ');//.replace(/ /g, '.*');
            const newRegesForSearch = searchQuery.map(function (cureentValue) {
                return new RegExp('^' + cureentValue, 'i');
            });
            searchCriteria['answer'] = { $in: newRegesForSearch };
        }
        if (requestData.label !== undefined && requestData.label != '') {
            searchCriteria['label'] = requestData.label;
        }
        if (requestData.symbol !== undefined && requestData.symbol != '') {
            searchCriteria['symbol'] = requestData.symbol;
        }
        if (requestData.visible !== undefined && requestData.visible != '') {
            searchCriteria['visible'] = requestData.visible;
        }
        //for Report
        //summary
        if (requestData.from != undefined && requestData.from != null && requestData.from != "null" && requestData.from != '' && requestData.to != undefined && requestData.to != null && requestData.to != "null" && requestData.to != '') {
            let from = requestData.from;
            let to = requestData.to;
            // from = from + 'T00:00:00.000Z';
            // to = to + 'T23:59:59.000Z';
            searchCriteria['createdAt'] = {
                $gte: new Date(from),
                $lt: new Date(to)
                // $gte: new Date(from).toISOString(),
                // $lt: new Date(to).toISOString()
            };
        }
        if (requestData.from_completion != undefined && requestData.from_completion != null && requestData.from_completion != '' && requestData.to_completion != undefined && requestData.to_completion != null && requestData.to_completion != '') {
            let from_completion = requestData.from_completion;
            let to_completion = requestData.to_completion;
            /*from_completion = from_completion + 'T00:00:00.000Z';
            to_completion = to_completion+ 'T23:59:59.000Z';*/
            searchCriteria['SJT.completionDateTime'] = {
                $gte: from_completion + 'T00:00:00.000Z',
                $lt: to_completion + 'T23:59:59.000Z'
            };
        }
        if (requestData.completed != undefined && requestData.completed != '') {
            searchCriteria['completed'] = requestData.completed;
        }
        //Data
        if (requestData.emails != undefined && requestData.emails != null && requestData.emails.length > 0) {
            let emailArray = requestData.emails.split(',');
            searchCriteria['email'] = {
                $in: emailArray
            }
        }
        //Personality List
        if (requestData.result != undefined && requestData.result != '') {
            searchCriteria['result'] = requestData.result;
        }
        //Logical Reasoning
        if (requestData.sync_from != undefined && requestData.sync_from != null && requestData.sync_from != '' && requestData.sync_to != undefined && requestData.sync_to != null && requestData.sync_to != '') {
            let from = requestData.sync_from;
            let to = requestData.sync_to;
            searchCriteria['QuestionsOutputs.lastSentOn'] = {
                $gte: new Date(from),
                $lt: new Date(to)
            };
        }
        if (requestData.start_from != undefined && requestData.start_from != null && requestData.start_from != '' && requestData.start_to != undefined && requestData.start_to != null && requestData.start_to != '') {
            let from = requestData.start_from;
            let to = requestData.start_to;
            searchCriteria['QuestionsOutputs.createdAt'] = {
                $gte: new Date(from),
                $lt: new Date(to)
            };
        }
        return searchCriteria;
    },


};

module.exports = Common;
