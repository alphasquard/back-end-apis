const fs = require('fs'),
    path = require('path'),
    CryptoData = require(path.resolve('./lib/crypto-data')),
    jwt = require("jsonwebtoken"),
    algorithms = ["HS256", "HS384", "HS512", "RS256", "RS384", "RS512"];

const privateKey = fs.readFileSync(path.resolve(global.config.secret));
const expiresIn = (60 * 60 * 60);
var JWT = {

    generateJwtToken: function (req, data) {

        let encryptData = CryptoData.encryptWithIv(Buffer.from(data._id.toString('hex'), "utf8"));
        let payload = {
            data: encryptData.prv
        };

        //let expiresIn = (60*60*60);
        console.log(req.hostname);

        const options = {
            //algorithms: algorithms,
            issuer: req.hostname,
            expiresIn: expiresIn,
            jwtid: encryptData.iv,
            audience: data.for
        };

        let token = jwt.sign(payload, privateKey, options);
        return { token, expiresIn }
    },

    verifyJwtToken: function (req) {
        try {
            let token = JWT.getToken(req);
            if (token) {
                const options = { issuer: req.hostname }; //{ issuer: 'alpha.develop.imran' };
                var decoded = jwt.verify(token, privateKey, options);
                let decrypt = CryptoData.decryptWithIv({
                    prv: decoded.data,
                    iv: decoded.jti
                });
                decoded._id = decrypt;
                return decoded;
            }
        } catch (e) {
            console.log(e.message);
            return false;//res.status(403).send({ status: false, message: 'Unauthorized request. Access denied'});
        }
        return false;//res.status(403).send({ status: false, message: 'Unauthorized request. Access denied'});
    },

    getToken: function (req) {
        try {
            if (req.headers['authorization'] || req.headers['Authorization']) {
                return req.headers['authorization'].split(' ')[1];
            }
            if (req.cookies.authorization != undefined && req.cookies.authorization != null && req.cookies.authorization != '') {
                return req.cookies.authorization;
            }
            if (req.query.token != undefined && req.query.token != null && req.query.token != '') {
                return req.query.token;
            }
        } catch (e) {
            console.log(e.message);
            return false;
        }
        return false;
    },

    refreshToken: function (token) {
        const payload = jwt.decode(token, privateKey);
        const dateNow = new Date();
        const nowTime = dateNow.getTime() / 1000;
        if (payload.exp < nowTime) {
            return JWT.generateJwtToken(payload);
        } else {
            return {
                token,
                expiresIn: parseInt(payload.exp - nowTime)
            };
        }
    },

    destroyJwt: function (req) {
        req.session.jwt = '';
        return req.session.jwt;
    },
    tokenValidation: (req, res) => {
        let returnData = {};
        let user = req.user;
        const currentToken = JWT.getToken(req);
        const token = JWT.refreshToken(currentToken);
        returnData.token = token;
        returnData.user = user;
        returnData.status = true;
        return res.json(returnData);
    },
};
module.exports = JWT;
