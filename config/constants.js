module.exports = {

  challenges: {
    game1: {
      'slug': 'trust-fairness',
      level: 1,
      individualTraits: {
        'trust': 'Trustworthy', 'fairness': 'Fair'
      }
    },
    game2: {
      'slug': 'reinforcement-learning',
      level: 1,
      individualTraits: {
        'quickLearner': 'Quick Learner'
      }
    },
    game3: {
      'slug': 'comfort-with-ambiguity',
      level: 1,
      individualTraits: {
        'descisive': 'Descisive'
      }
    },
    game4: {
      'slug': 'creativity',
      level: 1,
      individualTraits: {
        'creative': 'Creative'
      }
    },
    game5: {
      'slug': 'planning',
      level: 1,
      individualTraits: {
        'carefulPlanner': 'Careful planner', 'futureThinker': 'Future thinker'
      }
    },
    game6: {
      'slug': 'working-memory',
      level: 1,
      individualTraits: {
        'eideticMemory': 'Eidetic Memory'
      }
    },
    game7: {
      'slug': 'attention',
      level: 1,
      individualTraits: {
        'focus': 'Focus', 'attention': 'Attention'
      }
    },
    game8: {
      'slug': 'effort',
      level: 1,
      individualTraits: {
        'diligent': 'Diligent'
      }
    },
    game9: {
      'slug': 'risk-taking-propensity',
      level: 1,
      individualTraits: {
        'riskProfile': 'Risk Profile'
      }
    },
    game10: {
      'slug': 'risk-behavior',
      level: 1,
      individualTraits: {
        'riskAssessor': 'Risk Assessor'
      }
    },
    game11: {
      'slug': 'altruism-idealism',
      level: 1,
      individualTraits: {
        'selfless': 'Selfless', 'principled': 'Principled'
      }
    },
    game12: {
      'slug': 'logical-reasoning',
      level: 1,
      individualTraits: {
        'logicalThinker': 'Logical Thinker'
      }
    },
    game13: {
      'slug': 'personality-self-assessment',
      level: 1
    },
    /* game14: {
         'slug': ''
     },
     game15: {
         'slug': ''
     },
     game16: {
         'slug': ''
     },
     game17: {
         'slug': ''
     },
     game18: {
         'slug': ''
     }*/

  },
  reports: {
    stageOne: {
      decision: {
        heading: '',
        description: ''
      },
      pageOne: {
        label: 'Working on tasks',
        slug: 'working_on_tasks',
        sort_no: 1,
        desc: 'This section describes the candidate\'s behavior when receiving, approaching, and solving a task at work.',
        totalAvg: 0,
        first_avg: {
          label: "Average of Conscientiousness and Planning",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 7.4324791
            },
            "medium": {
              "operator": "<",
              "value": 20.56384808
            },
            "high": {
              "operator": ">=",
              "value": 20.56384808
            }
          }
        },
        second_avg: {
          label: "Average of Verbal Reasoning, Numerical Reasoning, Working with Information, Refrain control, and Accurate Attention",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 48.13638792
            },
            "medium": {
              "operator": "<",
              "value": 67.91866438
            },
            "high": {
              "operator": ">=",
              "value": 67.91866438
            }
          }
        },
        criteria: {
          "low": {
            "operator": "<",
            "value": 37.82152405
          },
          "medium": {
            "operator": "<",
            "value": 53.0738925
          },
          "high": {
            "operator": ">=",
            "value": 53.0738925
          }
        },
        section: {
          one: {
            label: 'Conscientiousness',
            slug: 'conscientiousness',
            statementSlug: 'conscientiousness',
            sort_no: 1.1,
            desc: 'Paying attention to details, planning thoroughly and enjoying order, as opposed to focusing on the end objective, enjoying uncertainty and acting without a plan.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 5.90
              },
              "medium": {
                "operator": "<",
                "value": 16.11
              },
              "high": {
                "operator": ">=",
                "value": 16.11
              }
            },
            haveCalculation: true,
            childs: {
              one: {
                label: 'Achievement-striking',
                slug: 'achievement-striking',
                statementSlug: 'achievement_striking',
                sort_no: '1.1.1',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 0.86
                  },
                  "medium": {
                    "operator": "<",
                    "value": 3.61
                  },
                  "high": {
                    "operator": ">=",
                    "value": 3.61
                  }
                },
              },
              two: {
                label: 'Orderliness',
                slug: 'orderliness',
                statementSlug: 'orderliness',
                sort_no: '1.1.2',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 1.01
                  },
                  "medium": {
                    "operator": "<",
                    "value": 5.57
                  },
                  "high": {
                    "operator": ">=",
                    "value": 5.57
                  }
                },
              },
              three: {
                label: 'Self-discipline',
                slug: 'self-discipline',
                statementSlug: 'self_discipline',
                sort_no: '1.1.3',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 3.03
                  },
                  "medium": {
                    "operator": "<",
                    "value": 7.93
                  },
                  "high": {
                    "operator": ">=",
                    "value": 7.93
                  }
                },
              }
            }
          },
          two: {
            label: 'Planning',
            slug: 'planning',
            statementSlug: 'planning',
            sort_no: 1.2,
            desc: 'Identifying the activities needed to successfully reach a goal.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 4.78
              },
              "medium": {
                "operator": "<",
                "value": 29.27
              },
              "high": {
                "operator": ">=",
                "value": 29.27
              }
            }
          },
          three: {
            label: 'Accurate Attention',
            slug: 'accurate_attention',
            statementSlug: 'accurateAttention',
            sort_no: 1.3,
            desc: 'Staying alert over time to detect important information in the environment and act accordingly.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 74.59
              },
              "medium": {
                "operator": "<",
                "value": 110.64
              },
              "high": {
                "operator": ">=",
                "value": 110.64
              }
            }
          },
          four: {
            label: 'Refrain Control',
            slug: 'refrain_control',
            statementSlug: 'refrainControl',
            sort_no: 1.4,
            desc: 'Detecting similar but different information and refraining from acting when it\'s not needed.',
            criteria: {
              "low": {
                "operator": "<",
                "value": -7.43
              },
              "medium": {
                "operator": "<=",
                "value": 27.56
              },
              "high": {
                "operator": ">",
                "value": 27.56
              }
            }
          },
          five: {
            label: 'Working with information',
            slug: 'working_with_information',
            statementSlug: 'working_with_information',
            sort_no: 1.5,
            desc: 'Temporarily storing and manipulating information in the face of ongoing processing and distraction.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 43.58
              },
              "medium": {
                "operator": "<",
                "value": 98.99
              },
              "high": {
                "operator": ">=",
                "value": 98.99
              }
            }
          },
          six: {
            label: 'Numerical Reasoning',
            slug: 'numerical_reasoning',
            statementSlug: 'numericalReasoning',
            sort_no: 1.6,
            desc: 'Easily understanding, processing and transforming quantitative information.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 37.90
              },
              "medium": {
                "operator": "<",
                "value": 76.72
              },
              "high": {
                "operator": ">=",
                "value": 76.72
              }
            }
          },
          seven: {
            label: 'Verbal Reasoning',
            slug: 'verbal_reasoning',
            statementSlug: 'verbalReasoning',
            sort_no: 1.7,
            desc: 'Understanding in depth and operating with qualitative information.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 44.73
              },
              "medium": {
                "operator": "<",
                "value": 80.50
              },
              "high": {
                "operator": ">=",
                "value": 80.50
              }
            }
          }
        },
      },
      pageTwo: {
        label: 'Learning',
        slug: 'learning',
        statementSlug: 'learning',
        sort_no: 2,
        desc: 'This section describes the candidate\'s behavior when faced with new information or unknown tasks, for which the candidate must develop new methods in order to successfully solve the task.',
        totalAvg: 0,
        first_avg: {
          label: "Average of Openness and Comfort with Ambiguity",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 12.76606605
            },
            "medium": {
              "operator": "<",
              "value": 39.84939913
            },
            "high": {
              "operator": ">=",
              "value": 39.84939913
            }
          }
        },
        second_avg: {
          label: "Average of Creativity and Logical Reasoning",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 50.91230789
            },
            "medium": {
              "operator": "<",
              "value": 74.7930738
            },
            "high": {
              "operator": ">=",
              "value": 74.7930738
            }
          }
        },
        criteria: {
          "low": {
            "operator": "<",
            "value": 35.41950914
          },
          "medium": {
            "operator": "<",
            "value": 53.7409143
          },
          "high": {
            "operator": ">=",
            "value": 53.7409143
          }
        },
        section: {
          one: {
            label: 'Openness',
            slug: 'openness',
            statementSlug: 'openness',
            sort_no: 2.1,
            desc: 'Being creative, curious and enjoying uncertainty vs. respecting the status quo and enjoying certainty.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 3.91
              },
              "medium": {
                "operator": "<",
                "value": 13.74
              },
              "high": {
                "operator": ">=",
                "value": 13.74
              }
            },
            haveCalculation: true,
            childs: {
              one: {
                label: 'Imagination',
                slug: 'imagination',
                statementSlug: 'imagination',
                sort_no: '2.1.1',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 1.58
                  },
                  "medium": {
                    "operator": "<",
                    "value": 5.06
                  },
                  "high": {
                    "operator": ">=",
                    "value": 5.06
                  }
                }
              },
              two: {
                label: 'Ambiguity',
                slug: 'ambiguity',
                statementSlug: 'ambiguity',
                sort_no: '2.1.2',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 1.07
                  },
                  "medium": {
                    "operator": "<",
                    "value": 6.06
                  },
                  "high": {
                    "operator": ">=",
                    "value": 6.06
                  }
                }
              },
              three: {
                label: 'Curiosity',
                slug: 'curiosity',
                statementSlug: 'curiosity',
                sort_no: '2.1.3',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 0.21
                  },
                  "medium": {
                    "operator": "<",
                    "value": 3.65
                  },
                  "high": {
                    "operator": ">=",
                    "value": 3.65
                  }
                }
              }
            },
          },
          two: {
            label: 'Comfort with Ambiguity',
            slug: 'comfort_with_ambiguity',
            statementSlug: 'comfort_with_ambiguity',
            sort_no: 2.2,
            desc: 'Recognising uncertain information and interpreting it with optimism.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 17.43
              },
              "medium": {
                "operator": "<",
                "value": 66.89
              },
              "high": {
                "operator": ">=",
                "value": 66.89
              }
            }
          },
          three: {
            label: 'Creativity',
            slug: 'creativity',
            statementSlug: 'creativity',
            sort_no: 2.3,
            desc: 'Coming up with many original ideas related to an issue.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 55.75
              },
              "medium": {
                "operator": "<",
                "value": 81.67
              },
              "high": {
                "operator": ">=",
                "value": 81.67
              }
            }
          },
          four: {
            label: 'Logical Reasoning',
            slug: 'logical_reasoning',
            statementSlug: 'logicalReasoning',
            sort_no: 2.4,
            desc: 'Understanding abstract information, identifying patterns, using deductive and inductive logic.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 41.30
              },
              "medium": {
                "operator": "<",
                "value": 78.87
              },
              "high": {
                "operator": ">=",
                "value": 78.87
              }
            }
          }
        },
      },
      pageThree: {
        label: 'Working with others',
        slug: 'working_with_others',
        statementSlug: 'working_with_others',
        sort_no: 3,
        desc: 'This section describes the candidate\'s behavior when working with clients or in teams, either as team member or leader. Generally refers to the situations in which some form of social interaction is required in order to solve a task.',
        totalAvg: 0,
        first_avg: {
          label: "Average Score of Agreeableness, Extraversion, Altruism and Trust",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 7.682878052
            },
            "medium": {
              "operator": "<",
              "value": 23.84622427
            },
            "high": {
              "operator": ">=",
              "value": 23.84622427
            }
          }
        },
        second_avg: {
          label: "Average Score of Fairness and Flexibility",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 21.24197167
            },
            "medium": {
              "operator": "<",
              "value": 48.50598532
            },
            "high": {
              "operator": ">=",
              "value": 48.50598532
            }
          }
        },
        criteria: {
          "low": {
            "operator": "<",
            "value": 15.08352255
          },
          "medium": {
            "operator": "<",
            "value": 29.185198
          },
          "high": {
            "operator": ">=",
            "value": 29.185198
          }
        },
        section: {
          one: {
            label: 'Agreeableness',
            slug: 'agreeableness',
            statementSlug: 'agreeableness',
            sort_no: 3.1,
            desc: 'Having empathy, being altruistic and cooperative vs. focusing on own interest, being rational and cold.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 11.45
              },
              "medium": {
                "operator": "<",
                "value": 29.91
              },
              "high": {
                "operator": ">=",
                "value": 29.91
              }
            },
            haveCalculation: true,
            childs: {
              one: {
                label: 'Empathy',
                slug: 'empathy',
                statementSlug: 'empathy',
                sort_no: '3.1.1',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 1.73
                  },
                  "medium": {
                    "operator": "<",
                    "value": 6.03
                  },
                  "high": {
                    "operator": ">=",
                    "value": 6.03
                  }
                },
              },
              two: {
                label: 'Altruism',
                slug: 'altruism',
                statementSlug: 'altruism',
                sort_no: '3.1.2',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 2.40
                  },
                  "medium": {
                    "operator": "<",
                    "value": 6.72
                  },
                  "high": {
                    "operator": ">=",
                    "value": 6.72
                  }
                }
              },
              three: {
                label: 'Cooperation',
                slug: 'cooperation',
                statementSlug: 'cooperation',
                sort_no: '3.1.3',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 0.75
                  },
                  "medium": {
                    "operator": "<",
                    "value": 3.04
                  },
                  "high": {
                    "operator": ">=",
                    "value": 3.04
                  }
                },
              }
            },
          },
          two: {
            label: 'Extraversion',
            slug: 'extraversion',
            statementSlug: 'extraversion',
            sort_no: 3.2,
            desc: 'Being talkative, sociable and taking the lead as opposed to  being quiet, drawn and following others.',
            criteria: {
              "low": {
                "operator": "<",
                "value": -0.57
              },
              "medium": {
                "operator": "<",
                "value": 11.25
              },
              "high": {
                "operator": ">=",
                "value": 11.25
              }
            },
            haveCalculation: true,
            childs: {
              one: {
                label: 'Assertiveness',
                slug: 'assertiveness',
                statementSlug: 'assertiveness',
                sort_no: '3.2.1',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": -0.57
                  },
                  "medium": {
                    "operator": "<",
                    "value": 3.81
                  },
                  "high": {
                    "operator": ">=",
                    "value": 3.81
                  }
                }
              },
              two: {
                label: 'Friendliness',
                slug: 'friendliness',
                statementSlug: 'friendliness',
                sort_no: '3.2.2',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": -0.01
                  },
                  "medium": {
                    "operator": "<",
                    "value": 4.95
                  },
                  "high": {
                    "operator": ">=",
                    "value": 4.95
                  }
                }
              },
              three: {
                label: 'Sociability',
                slug: 'sociability',
                statementSlug: 'sociability',
                sort_no: '3.2.3',
                desc: '',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": -0.78
                  },
                  "medium": {
                    "operator": "<",
                    "value": 3.28
                  },
                  "high": {
                    "operator": ">=",
                    "value": 3.28
                  }
                }
              }
            },
          },
          three: {
            label: 'Altruism',
            slug: 'altruism',
            statementSlug: 'altruism',
            sort_no: 3.3,
            desc: 'Acting out of care for others, even by sacrificing own interest.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 4.18
              },
              "medium": {
                "operator": "<",
                "value": 28.48
              },
              "high": {
                "operator": ">=",
                "value": 28.48
              }
            }
          },
          //Game 1
          four: {
            label: 'Trust',
            slug: 'trust',
            statementSlug: 'trust',
            sort_no: 3.5,
            desc: 'Assuming that others will collaborate and behave in constructive ways to achieve common objectives.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 3.40
              },
              "medium": {
                "operator": "<",
                "value": 53.15
              },
              "high": {
                "operator": ">=",
                "value": 53.15
              }
            }
          },
          //Game 1
          five: {
            label: 'Fairness',
            slug: 'fairness',
            statementSlug: 'fairness',
            sort_no: 3.6,
            desc: 'Understanding differences in perspectives and counting them in when deciding how to act.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 40.42
              },
              "medium": {
                "operator": "<",
                "value": 94.63
              },
              "high": {
                "operator": ">=",
                "value": 94.63
              }
            }
          },
          six: {
            label: 'Flexibility',
            slug: 'flexibility',
            statementSlug: 'flexibility',
            sort_no: 3.4,
            desc: 'Adapting to the situation and acting contrary to own values when required.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 0.44
              },
              "medium": {
                "operator": "<",
                "value": 2.67
              },
              "high": {
                "operator": ">=",
                "value": 2.67
              }
            }
          },
        }
      },
      pageFour: {
        label: 'Motivation',
        slug: 'motivation',
        statementSlug: 'motivation',
        sort_no: 4,
        desc: 'This section describes the candidate\'s effort put into getting tasks done successfully while meeting deadlines, as well as how the candidate experiences working on different tasks.',
        totalAvg: 0,
        first_avg: {
          label: "Average of Emotionality, Learning based on Feedback, and Execution",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 13.76784343
            },
            "medium": {
              "operator": "<",
              "value": 37.72319599
            },
            "high": {
              "operator": ">=",
              "value": 37.72319599
            }
          }
        },
        second_avg: {
          label: "Average of Risk Taking, Evaluating Risk, and Effort",
          scoreStatement: '',
          desc: '',
          criteria: {
            "low": {
              "operator": "<",
              "value": 45.75123466
            },
            "medium": {
              "operator": "<",
              "value": 72.09461821
            },
            "high": {
              "operator": ">=",
              "value": 72.09461821
            }
          }
        },
        criteria: {
          "low": {
            "operator": "<",
            "value": 33.35246233
          },
          "medium": {
            "operator": "<",
            "value": 51.31598381
          },
          "high": {
            "operator": ">=",
            "value": 51.31598381
          }
        },
        section: {
          one: {
            label: 'Emotionality',
            slug: 'emotionality',
            statementSlug: 'emotionality',
            sort_no: 4.1,
            desc: 'Staying calm in difficult times, maintaining optimism and stability vs. easily feeling stressed and angry, being pessimistic',
            criteria: {
              "low": {
                "operator": "<",
                "value": -1.03
              },
              "medium": {
                "operator": "<",
                "value": 10.87
              },
              "high": {
                "operator": ">=",
                "value": 10.87
              }
            },
            haveCalculation: false,
            childs: {
              one: {
                label: 'Calm',
                slug: 'calm',
                statementSlug: 'calm',
                sort_no: '4.1.1',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": 0.32
                  },
                  "medium": {
                    "operator": "<",
                    "value": 5.09
                  },
                  "high": {
                    "operator": ">=",
                    "value": 5.09
                  }
                },
              },
              two: {
                label: 'Relaxation',
                slug: 'relaxation',
                statementSlug: 'relaxation',
                sort_no: '4.2.2',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": -0.74
                  },
                  "medium": {
                    "operator": "<",
                    "value": 2.38
                  },
                  "high": {
                    "operator": ">=",
                    "value": 2.38
                  }
                },
              },
              three: {
                label: 'Optimism',
                slug: 'optimism',
                statementSlug: 'optimism',
                sort_no: '4.3.3',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": -0.68
                  },
                  "medium": {
                    "operator": "<",
                    "value": 1.04
                  },
                  "high": {
                    "operator": ">=",
                    "value": 1.04
                  }
                },
              },
              four: {
                label: 'Stability',
                slug: 'stability',
                statementSlug: 'stability',
                sort_no: '4.4.3',
                criteria: {
                  "low": {
                    "operator": "<",
                    "value": -1.42
                  },
                  "medium": {
                    "operator": "<",
                    "value": 3.86
                  },
                  "high": {
                    "operator": ">=",
                    "value": 3.86
                  }
                },
              }
            },
          },
          two: {
            label: 'Execution',
            slug: 'execution',
            statementSlug: 'execution',
            sort_no: 4.2,
            desc: 'Acting according to plan and reaching the anticipated results.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 5.83
              },
              "medium": {
                "operator": "<",
                "value": 19.32
              },
              "high": {
                "operator": ">=",
                "value": 19.32
              }
            }
          },
          three: {
            label: 'Effort',
            slug: 'effort',
            statementSlug: 'effort',
            sort_no: 4.3,
            desc: 'Willingness to put in extra effort to achieve results.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 15.28
              },
              "medium": {
                "operator": "<",
                "value": 71.04
              },
              "high": {
                "operator": ">=",
                "value": 71.04
              }
            }
          },
          four: {
            label: 'Risk Taking',
            slug: 'risk_taking',
            statementSlug: 'risk_taking',
            sort_no: 4.4,
            desc: 'Preferring uncertain, but higher rewards to certain, lower rewards.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 75.54
              },
              "medium": {
                "operator": "<",
                "value": 99.46
              },
              "high": {
                "operator": ">=",
                "value": 99.46
              }
            }
          },
          five: {
            label: 'Evaluating Risk',
            slug: 'evaluating_risk',
            statementSlug: 'evaluating_risk',
            sort_no: 4.5,
            desc: 'Correctly evaluating the degree of risk involved in different situations.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 26.86
              },
              "medium": {
                "operator": "<",
                "value": 68.87
              },
              "high": {
                "operator": ">=",
                "value": 68.87
              }
            }
          },
          six: {
            label: 'Learning Based on Feedback',
            slug: 'learning_based_on_feedback',
            statementSlug: 'learning_based_on_feedback',
            sort_no: 4.6,
            desc: 'Adapting own actions as a consequence of positive rewards received.',
            criteria: {
              "low": {
                "operator": "<",
                "value": 26.17
              },
              "medium": {
                "operator": "<",
                "value": 93.83
              },
              "high": {
                "operator": ">=",
                "value": 93.83
              }
            }
          },
          seven: {
            label: 'Growth Mindset',
            slug: 'growth_mindset',
            statementSlug: 'growth_mindset',
            sort_no: 4.7,
            desc: '',
            criteria: {
              "low": {
                "operator": "<",
                "value": 26.17
              },
              "medium": {
                "operator": "<",
                "value": 93.83
              },
              "high": {
                "operator": ">=",
                "value": 93.83
              }
            }
          }
        }
      }
    },
    stageOneCandidate: {
      decision: {
        heading: '',
        description: ''
      },
      pageOne: {
        label: 'Your Individual Profile',
        slug: 'your_individual_profile',
        sort_no: 1,
        desc: '',
        totalAvg: 0,
        section: {
          one: {
            label: 'Working on tasks',
            slug: 'working_on_tasks',
            sort_no: 1.1,
            desc: 'Description of how you typically behave when receiving, approaching, and solving a task at work. For example, when you receive data on market trends and must compile a report independently.',
            statements: {
              candp: {
                name: 'Average of Conscientiousness and Planning',
                low: 'You are usually spontaneous and don\'t lose time planning before acting, and ambiguous tasks don\'t discourage you. Your focus is usually the big picture, not the details. You usually stay engaged as long as you find the work interesting.',
                medium: 'When faced with complex tasks, you take time to plan before action, but you tend to be spontaneous in approaching easy tasks. You usually stay focused on your work and get most of the details right. You tend to avoid very tedious or unclear tasks. ',
                high: 'You almost always take time to plan and approach tasks mindfully. You tend to stay focused even when tasks get tedious and you have great attention to details. Very ambiguous or unclear tasks might discourage you.'
              },
              vrnrwwiecaaa: {
                name: 'Average of Verbal Reasoning, Numerical Reasoning, Working with Information, Refrain control, and Accurate Attention',
                low: 'You are able to use information to correctly solve easy problems, but might be baffled by complex problems.',
                medium: 'You are able to use information to solve moderately difficult problems, but you might struggle with very complex tasks.',
                high: 'You are able to work with large amounts of information to solve very complex problems correctly.'
              }
            }
          },
          two: {
            label: 'Learning',
            slug: 'learning',
            sort_no: 1.2,
            desc: 'Description of how you tend to behave when faced with new information or unknown tasks, for which you are required to develop new methods in order to successfully solve them. For example, when you are asked to sell a product you have never sold before.',
            statements: {
              oacwa: {
                name: 'Average of Openness and Comfort with Ambiguity',
                low: 'You prefer to solve tasks by applying strategies that you know well and have used before. You usually decide on your own how to best approach a problem.',
                medium: 'You solve most tasks by applying the strategy you know best and have used before. For novel problems, you might ask and listen to others\' ideas on how to proceed.',
                high: 'You usually explore many different possibilities before committing to one for solving a task. You will consider others\' ideas and will take time to think of different alternatives of action.'
              },
              calr: {
                name: 'Average of Creativity and Logical Reasoning',
                low: 'You usually require time to learn how to solve new tasks. You are usually satisfied with a single solution to a problem.',
                medium: 'You learn how to solve new tasks fairly quickly. Usually, you can come up with a few different solutions to a problem.',
                high: 'You easily learn how to solve even complex tasks. You can come up with many different solutions to a problem.'
              }
            }
          },
          three: {
            label: 'Working in teams',
            slug: 'working_in_teams',
            sort_no: 1.3,
            desc: 'Description of how you usually behave when working with clients or in teams, either as team member or leader. Generally refers to the situations in which you must interact with other people in order to solve a task. For example, when you have to offer customer support over the phone.',
            statements: {
              soaeat: {
                name: 'Average Score of Agreeableness, Extraversion, Altruism and Trust',
                low: 'You usually enjoy working alone all day. You are self-reliant and don\'t count on others to get work done.',
                medium: 'You don\'t mind working alone, but you like to interact with others from time to time. You are mostly self-reliant, but you count on others\' help when you\'re under pressure.',
                high: 'You enjoy working with other people, talking and listening to them. You often solve problems through collaboration, counting on others\' help.'
              },
              soff: {
                name: 'Average Score of Fairness and Flexibility',
                low: 'You always stay true to your own values and are hard to convince. You can easily confront others openly about their actions.',
                medium: 'You tend to stay true to your own values, but you can adapt to others\' perspectives if needed. You believe that most people are well intentioned.',
                high: 'You are tolerant, understanding others\' perspectives and adapting to them. You believe everyone has good intentions.'
              }
            }
          },
          four: {
            label: 'Staying motivated',
            slug: 'staying_motivated',
            sort_no: 1.2,
            desc: 'A description of how likely you are to put in the effort required to get tasks done successfully while meeting deadlines, as well as how you deal with setbacks. For example, when the code you wrote doesn’t work.',
            statements: {
              elbofae: {
                name: 'Average of Emotionality, Learning based on Feedback, and Execution',
                low: 'You tend to plan for the worst and you easily get upset by difficulties. When faced with adversities, you change your plan and adapt.',
                medium: 'You tend to face problems with optimism, but you get upset by strong difficulties and try to adapt the way you work as a consequence.',
                high: 'You almost always face problems with optimism and don\'t let difficulties upset you, focusing on doing your best to achieve results. You see mistakes as an opportunity to learn.'
              },
              rterae: {
                name: 'Average of Risk Taking, Evaluating Risk, and Effort',
                low: 'You put in the required effort to achieve guaranteed results, but you don\'t like to make an effort if success isn\'t ensured.',
                medium: 'You can put in extra effort to aim for better results, but only if there\'s a good chance of succeeding.',
                high: 'You often put in extra effort and try to aim for the best results, even if success is not probable.'
              }
            }
          }
        },
      },
      pageTwo: {
        label: 'Tailored Recommendations',
        slug: 'tailored_recommendations',
        sort_no: 2,
        desc: '',
        totalAvg: 0,
        section: {
          one: {
            label: 'Approaching tasks and organizing your work',
            slug: 'approaching_tasks_and_organizing_your_work',
            sort_no: 2.1,
            desc: 'Based on your assessment results, we offer a few tips on how to better approach tasks and organize your work.',
            statements: {
              cpvrnrwwircaaa: {
                name: 'Average of Conscientiousness, Planning, Verbal Reasoning, Numerical Reasoning, Working with Information, Refrain control, and Accurate Attention',
                low: 'Your results indicate that you easily get bored when work gets hard and tedious. However, few jobs nowadays offer interesting tasks continuously. So, when you will have to get work done even if you don\'t enjoy it, try to:\n' +
                  '- break down the task in smaller and easier sub-tasks. This will make it more approachable.\n' +
                  '- divide the task in small chunks and aim to finish one chunk at a time. Reward yourself (with a break or a sweet treat) everytime you finish one chunk of the work.\n' +
                  '- tell others what your goals are: what you want to achieve and by when. This can help you stay on track.\n' +
                  '- review your work and check for errors. Get someone else to help you reviewing at first. This might help you get used to checking the quality of your work in detail.',
                medium: 'Your results indicate that you can follow a plan, but you get bored if work gets too tedious. When faced with tedious tasks, try to:\n' +
                  '- divide the task in small chunks and aim to finish one chunk at a time. Reward yourself (with a break or a sweet treat) everytime you finish one chunk of the work.\n' +
                  '- tell others what your goals are: what you want to achieve and by when. This can help you stay on track.\n' +
                  '- plan from the start how you will review your work at the end (e.g. how much time you will dedicate to it). This will make it more likely that you will do it.',
                high: 'Your results indicate that you like clear instructions and plans. However, today\'s work environment is highly unpredictable, so often we need to act quickly, without knowing the next steps. When faced with urgent and ambiguous tasks, try to:\n' +
                  '- define the main objective you must achieve and what output is expected from you. These few details should make it easier to start working.\n' +
                  '- decide what is the milestone that you\'re working on for now, and focus on achieving it. Then you can re-evaluate the problem and gather more information. This should help you get to work quickly.\n' +
                  ' - when you completed a deliverable, set limits to the review (e.g. an amount of time you\'ll dedicate or the number of times you will reread it). This way, you can avoid getting stuck in fixing all the unimportant details.'
              }
            }
          },
          two: {
            label: 'Dealing with new and unknown tasks',
            slug: 'dealing_with_new_and_unknown_tasks',
            sort_no: 2.2,
            desc: 'Based on your assessment results, we give you a few recommendations on how to approach unknown tasks that you’ve never done before.',
            statements: {
              oacwwacalr: {
                name: 'Average of Openness and Comfort with Ambiguity, Creativity and Logical Reasoning',
                low: 'Your results indicate that you prefer solving problems by using strategies that you are familiar with. However, nowadays the world is quickly changing and many jobs require people to constantly develop and learn new things. When facing new challenges at work, try to:\n' +
                  '- think of past changes you went through and list what benefits they brought for you. This might help you be more open to change.\n' +
                  '- ask at least two other people\'s opinion on how to proceed. This will ensure you have more than one alternative to choose from.\n' +
                  '- before taking action, write down the benefits and the risks of the approach you\'re considering. This process will likely help you come up with alternative ideas to mitigate the risks.\n' +
                  '- intentionally choose to do something new that you\'ve never done before. This way you will get used to learning new things and overcoming the initial hesitation.',
                medium: 'Your results indicate that you usually solve problems through well-known strategies, but sometimes you enjoy experimenting and learning. Many jobs require carefully balancing the two. To recognize when to innovate and when to apply established solutions, try to:\n' +
                  '- identify if the problem is old and search for a tried-and-tested solution in internal documents. This might help you gain time and efficiency.\n' +
                  '- write down the risks and benefits of the alternative solutions you came up with. This might help you choose better solutions.\n' +
                  '- before starting to learn new things in order to solve a problem, make a list of the necessary knowledge and skills that you must acquire. This will help you stay focused in your exploration.',
                high: 'Your results indicate that you enjoy experimenting and learning new ways to solve problems. However, many jobs require the repeated application of the same procedures and only sometimes allow for innovation. When faced with routine tasks, try to:\n' +
                  '- identify if the problem is old and search for a tried-and-tested solution in internal documents. This might help you gain time and efficiency.\n' +
                  '- write down the risks and benefits of the alternative solutions you came up with. This might help you choose better solutions.\n' +
                  '- ask another person if they think there is room for improvement in how the problem you\'re facing is usually tackled. This can help you get a neutral opinion on the need for exploration.\n' +
                  '- before starting to learn new things in order to solve a problem, make a list of the necessary knowledge and skills that you must acquire. This will help you stay focused in your exploration.',
              }
            }
          },
          three: {
            label: 'Working in teams',
            slug: 'working_in_teams',
            sort_no: 2.3,
            desc: 'Based on the results of the assessment, we offer a few tips to keep in mind when working in a team with other people. We also give you a few ideas to remember when you are the leader of a team.',
            statements: {
              aeatfaf: {
                name: '(Average of Agreeableness, Extraversion, Altruism, Trust, Fairness and Flexibility)',
                low: 'Your results indicate that you prefer to work alone and might avoid collaboration. However, almost all jobs nowadays ask us to work with others, and with good reason: teams are usually more effective than their best single member. So, when you find yourself in a team context, try to:\n' +
                  '- look for information about the benefits of teams and why so much of our work is done through collaboration. This might help you accept the necessity of teams.\n' +
                  '- list the things you and your team members have in common (educational background, taste in music, etc). This might help you feel closer to them.\n' +
                  '- identify how each person in the team, including you, can contribute to the team\'s objective. This might help you see your team as a cohesive group of people with complementary skills.' +
                  '\n ' +
                  'If you are leading a team, you should pay attention to the following points in order to become a more effective leader:\n' +
                  '- ask the team members about the values they hold important. Discuss why they do so and how it affects their behavior at work. This should help you be more aware and tolerant of other people\'s values.\n' +
                  '- engage the team in some non-task related activity, like getting lunch together or playing a teambuilding game. This should help you feel closer to the team members and develop your empathy.',
                medium: 'Your results indicate that you feel best mixing individual and team work. However, some tasks or projects require an intensive period of only one way of working, wither individual or collaborative. To balance the two, try to:\n' +
                  '- list the tasks you have to do alone and those you must collaborate with others to complete. Schedule your work so that they alternate and during one day, you can both be by your own and work with others.\n' +
                  '- when you have to work in a team for a longer time, list the things you and your team members have in common (educational background, taste in music, etc). This might help you feel closer to them.\n' +
                  '- when you have to work alone for a longer period of time, make sure you schedule moments to be around your colleagues, like lunch or coffee breaks. This might prevent feelings of isolation.' +
                  '\n ' +
                  'If you are leading a team, you should pay attention to the following points in order to become a more effective leader:\n' +
                  '- identify the moments in which it\'s essential for you to be present in the team (e.g. weekly update meetings). Prioritise these occasions and only after that join the team for other occasions, if you feel like it.\n' +
                  '- schedule team meetings even if you\'re not in the same location, using video calls. This might help you feel connected to the team even from far away.',
                high: 'Your results indicate that you enjoy working in teams and interacting with other people. However, teams should be focused on getting work done and allowing everyone on the team to make valuable contributions. So, when you find yourself in a team context, try to:\n' +
                  '- identify your most shy and quiet team members. Write down the ways they have helped the team and their unique skills and knowledge. This might help you value them and their (rare) contributions more.\n' +
                  '- keep track of how much you contribute in team discussions. If it\'s too much, make an effort to refrain, especially when your ideas are not radically different from what is being discussed.\n' +
                  '- think of how much the other team members disclose about themselves and their internal states. If they do it much less than you do, calibrate your own disclosure. This way you might make them feel more comfortable.' +
                  '\n ' +
                  'If you are leading a team, you should pay attention to the following points in order to become a more effective leader:\n' +
                  '- keep track of how much each team member contributes to discussions. Encourage and give space to those who contribute less. This might help you keep in control your tendency to dominate conversations.\n' +
                  '- give team members the possibility to make their voice heard through written communication. Write an email asking for written input on a decision or send short anonymous feedback questionnaires. This might help people who are shy to contribute more.',
              }
            }
          }
        },
      },
      pageThree: {
        label: 'Best Fit Careers',
        slug: 'best_fit_careers',
        sort_no: 3,
        desc: 'Based on people’s different characteristics, some jobs and sectors might fit some better than others. Based on data about your personality, reasoning ability and traits that emerged during the assessment, we are able to identify the careers that are most likely to fit you.',
        totalAvg: 0,
        statements: {
          wot: {
            name: 'Working on tasks',
            low: 'Roles where work is varied and tasks are not very complex. Examples:\n' +
              '- Facility Manager\n' +
              '- Event Planner\n' +
              '- Recruiter',
            medium: 'Roles with moderately variate and complex tasks. Examples:\n' +
              '- UX/UI Designer\n' +
              '- Bookkeeping\n' +
              '- Telemarketer',
            high: 'Roles which require an eye for detail and careful execution of elaborate tasks. Examples:\n' +
              '- Lawyer\n' +
              '- Project Manager\n' +
              '- Data Scientist',
          },
          learning: {
            name: 'Learning',
            low: 'Roles where following procedures is key. Examples:\n' +
              '- Administrative Roles\n' +
              '- Quality Assurance Specialist\n' +
              '- Engineering Technician',
            medium: 'Roles where tasks are usually repetitive, but changes sometimes occur. Examples:\n' +
              '- Financial Analyst\n' +
              '- Mechanical Engineer\n' +
              '- Business Intelligence Specialist',
            high: 'Roles where change is frequent and tasks are complex. Examples:\n' +
              '- Technological Research & Development\n' +
              '- Chief Financial Officer\n' +
              '- Political Analyst',
          },
          wit: {
            name: 'Working in teams',
            low: 'Roles where collaboration is not required in order to be successful. Examples:\n' +
              '- Programmer\n' +
              '- Technical Documentation Writer\n' +
              '- Remote work positions',
            medium: 'Roles where occasional collaboration is needed. Examples:\n' +
              '- Auditor\n' +
              '- Trainer\n' +
              '- Designer',
            high: 'Client-facing roles where teamwork is required to be successful. Examples:\n' +
              '- Sales Agent\n' +
              '- Customer Support Specialist\n' +
              '- User Researcher',
          },
          motivation: {
            name: 'Motivation',
            low: 'Roles with very constant workload and emotionally-neutral outcomes. Examples: \n' +
              '- Technical Specialist\n' +
              '- Economist\n' +
              '- Reporting Specialist',
            medium: 'Roles with fairly constant workload and emotionally-neutral outcomes.\\ Examples:\n' +
              '- Roles in Support Functions (IT, HR, Finance, Marketing)',
            high: 'Roles which require risky decisions with high stakes and uncertainty. Examples:\n' +
              '- Forecasting\n' +
              '- Strategy\n' +
              '- Risk Management',
          }
        }
      },
    },
    stageTwo: {
      decision: {
        executive: {
          include: {
            heading: 'Include in Talent Pool',
            description: 'Following the assessment, the candidate has demonstrated the minimum level of competencies required to perform effectively in an Executive / Senior Executive role.'
          },
          exclude: {
            heading: 'Exclude from Talent Pool',
            description: 'Following the assessment, the candidate has failed to demonstrate the minimum level of competencies required to perform effectively in an Executive / Senior Executive role.'
          }
        },
        manager: {
          include: {
            heading: 'Include in Talent Pool',
            description: 'Following the assessment, the candidate has demonstrated the minimum level of competencies required to perform effectively in an Assistant Manager / Manager role.'
          },
          exclude: {
            heading: 'Exclude from Talent Pool',
            description: 'Following the assessment, the candidate has failed to demonstrate the minimum level of competencies required to perform effectively in an Assistant Manager / Manager role.'
          }
        }
      },
    },
   

  },
  scoreType: {
    low: 'low',
    medium: 'medium',
    high: 'high',
  },
  SJT: {
    type: {
      'executive': 'Executive/Senior Executive Level',
      'manager': 'Assistant Manager/Manager Level'
    }
  },
  roleId: '5e3bb5a775698235f0af75cf',
  logicalQuestionTypes: [
    {
      type: 'verbal',
      subType: ['analysis', 'analogy', 'antonym']
    },
    {
      type: 'numerical',
      subType: ['series', 'word_problems', 'least_value']
    }, {
      type: 'logical',
      subType: ['series', 'common_features', 'analogy']
    },
  ]
  

};
