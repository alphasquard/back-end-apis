var mongoConfig = {
  username: '',
  password: '',
  port: '27017',
  dbname: 'charity',
  host: '127.0.0.1'
}

module.exports = {
  // getConnectionString: function () {
  //   let credentialString = "";
  //   if (this.username != "") {
  //     credentialString = this.username + ':' + this.password + '@';
  //   }
  //   let connstr = 'mongodb://' + credentialString + this.host + ':' + this.port + '/' + this.dbname;
  //   return connstr;
  // }.bind(mongoConfig)
  getConnectionString:function () {
      return "mongodb://localhost/charity";
  }
  
}
