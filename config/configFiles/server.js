module.exports = {
  // port: normalizePort(process.env.PORT) || 3000
  port: process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
