module.exports = {
    s3: {
        accessKeyId: '',
        secretAccessKey: '',
        region: '',
        bucket: '',
        folders: {
            company: 'company/',
            invigilation: 'invigilation/',
            scenario: 'scenario/',
            userCv: 'user/cv',
            videoInterview: 'video-interview/',
        }
    }
};