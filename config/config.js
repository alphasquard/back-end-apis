var mongo = require('./configFiles/mongo').getConnectionString(),
  server = require('./configFiles/server'),
  awsConfig = require('./configFiles/aws-config'),
  auth = require('./auth.js');

module.exports = {
    //baseURL: 'localhost:3001/',
    baseURL: '',
    company: 'GEM Learning',
    projectName: 'CHARITY',
    projectDescription: '',
    mongo,
    server,
    secret: 'certificates/secret/private.ppk',//'ufoneGamifiedRecruitmentByAlphaSquared',
    auth,
    roles: {
        candidate: 'candidate',
        applicant: 'applicant',
        admin: 'admin',
        employee: 'employee'
    },
    email: {
        from : {
            name: 'GEM Learning',
            email: 'imran.khalid@alphasquared.co'
        }
    },
    locales : ['en', 'ur'],
    languages: {
        'en': {
            'name': 'English',
            'direction': 'ltr',
            'flag': '',
            'isCurrent': 1
        },
        'ur': {
            'name': 'Urdu',
            'direction': 'rtl',
            'flag': '',
            'isCurrent': 0
        }
    },
    s3: awsConfig.s3,
}
