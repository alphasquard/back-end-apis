
const express = require('express');
const path = require('path');
const glob = require('glob');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
// const VerifyCompany = require(path.resolve('./middleware/verifyCompany.js'));
// const errorHandler = require(path.resolve('./helpers/error-handler'));
const CORS = require(path.resolve('./middleware/cors.js'));
const cors = require('cors');
var app = express();
global.config.appObject = app;
//app.use('/api/*', CORS);
app.use(cors()) // include before other routes
//app.use(CORS);
//Database connectivity
const dbOptions = {
    // reconnectTries: Number.MAX_VALUE,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
};
console.log("app",process.env.MONGODB_URI || global.config.mongo);

var connectWithRetry = function () {
    // return mongoose.connect(global.config.mongo, dbOptions, function (err) {
    return mongoose.connect(process.env.MONGODB_URI || global.config.mongo, dbOptions, function (err) {
        if (err) {
            console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
            setTimeout(connectWithRetry, 5000);
        }
    });
};
connectWithRetry();
//var db = mongoose.createConnection(global.config.mongo, dbOptions);
//Get DB connection
var db = mongoose.connection;
//mongoose.set('useCreateIndex', true);


// This section is optional and used to configure twig.
app.set("twig options", {
    allow_async: true, // Allow asynchronous compiling
    strict_variables: false
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// parse cookies
app.use(cookieParser()); // cookie parser must use the same secret as express-session.
app.use('/uploads', express.static(path.join(__dirname, 'public/uploads')))
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'client/candidate')));
app.use('/panel', express.static(path.join(__dirname, 'client/company')));
app.use('/.well-known', express.static(path.join(__dirname, '.well-known'), { dotfiles: 'allow' }))


//check API with valid Domain
// app.use('/api/*', VerifyCompany);

//Load Routes
const routersAll = [];
let routers = glob.sync('./modules/*/*.routes.js');
routers.forEach(function (router) {
    routersAll.push(require(path.resolve(router))(app));
});

routers = glob.sync('./modules/*/*/*.routes.js');
routers.forEach(function (router) {
    routersAll.push(require(path.resolve(router))(app));
});
// global error handler
// app.use(errorHandler);

// for adding company
// app.get('*', async (req, res) => {
//     /* console.log('baseUrl: ', req.baseUrl);
//     console.dir('originalUrl: ', req.originalUrl) // '/admin/new'
//     console.dir('path: ', req.path) // '/new' */

//     let returnData = {message: 'Not found'};
//     let subDomains =  req.subdomains;
//     //req.subdomains, req.hostname, req.headers.host

//     if(!subDomains || !subDomains.length) {
//         return res.status(404).json(returnData);
//     }
//     //.select()
//     let checkCompanyExist = await Company.findOne({subDomain: subDomains[0], isActive: true, isEmailVerify: true}).lean().exec();
//     if(checkCompanyExist) {
//         req.company = checkCompanyExist;
//     } else  {
//         return res.sendFile('404.html', {root: path.join(__dirname, '/client/candidate/')});
//         //res.end()
//         //return res.status(404).json(returnData);
//     }
//     /* if(subDomains[1] !== undefined && subDomains[1] === 'panel') {
//         res.sendFile('index.html', {root: path.join(__dirname, '/client/company/')});
//         //res.sendFile(path.join(__dirname, '/client/company'), 'index.html');
//     } else */ 
//     if(subDomains[0] !== undefined) {
//         let originalUrl = req.originalUrl.split('/');
//         if(originalUrl[1] !== undefined && originalUrl[1] !== null && originalUrl[1] == 'panel') {
//             res.sendFile('index.html', {root: path.join(__dirname, '/client/company/')});
//         } else {
//             res.sendFile('index.html', {root: path.join(__dirname, '/client/candidate/')});
//         }
//         //res.sendFile(path.join(__dirname, '/client/candidate'), 'index.html');
//     } else {
//         res.sendFile('404.html', {root: path.join(__dirname, '/client/candidate/')});
//     }
//     //res.sendFile(path.join(__dirname, '/client/candidate'), 'index.html');
//     //res.end()
// });


module.exports = app;
